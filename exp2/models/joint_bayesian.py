from functools import partial
import time

from jax import numpy as jnp
from jax.lax import map as jmap
import numpy as np
from numpyro import enable_x64, sample, distributions as dist

from .bgp import bgp_model, bgp_predict_inner1
from .utils import (
    mcmc,
    normalize_training_data,
    normalize,
    denormalize,
    householder_reparameterization,
)

enable_x64()


###################################
# Numpyro model used for training #
###################################


def bfs_model(training_data, num_active_dims):
    # Problem dimensions
    m = num_active_dims
    n = training_data["x"].shape[1]
    num_projection_parameters = m * n - m * (m - 1) // 2

    # Priors
    projection_parameters = sample(
        "projection_parameters",
        dist.Normal(
            jnp.zeros((num_projection_parameters,)),
            jnp.ones((num_projection_parameters,)),
        ),
    )

    # Project inputs
    w = householder_reparameterization(projection_parameters, n, m)
    projected_training_data = {"x": training_data["x"] @ w, "y": training_data["y"]}
    return bgp_model(projected_training_data)


######################
# Prediction routine #
######################


# @partial(jit, static_argnums=3)
# @partial(vmap, in_axes=(None, 0, None, None), out_axes=(1, 1))
def bfs_predict_inner(pred_x, training_data, num_active_dims, posterior_draw):
    # Problem dimensions
    m = num_active_dims
    n = training_data["x"].shape[1]

    # Retrieve projection matrix
    projection_parameters = posterior_draw["projection_parameters"]
    w = householder_reparameterization(projection_parameters, n, m)

    # Project predictions sites
    projected_pred_x = pred_x @ w

    projected_training_data = {"x": training_data["x"] @ w, "y": training_data["y"]}

    return bgp_predict_inner1(projected_pred_x, projected_training_data, posterior_draw)


def bfs_predict(pred_x, posterior_draws, training_data, num_active_dims):
    means, variances = jmap(
        partial(bfs_predict_inner, pred_x, training_data, num_active_dims),
        posterior_draws,
    )
    return means.T, variances.T


######################################################
# High-Level Training/Prediction/Validation Routines #
######################################################


def train_joint_bayesian_model(training_data, training_parameters, dim_fs):
    # Normalize training data
    normalized_training_data, normalization_constants = normalize_training_data(
        training_data
    )

    # Sample from the posterior chains using MCMC
    start_time = time.time()
    posterior_draws = mcmc(
        bfs_model,
        (normalized_training_data, dim_fs),
        **training_parameters["mcmc_params"],
    )
    training_duration = time.time() - start_time

    return {
        "dim_feature_space": dim_fs,
        "posterior_draws": posterior_draws,
        "normalization_constants": normalization_constants,
        "training_duration": training_duration,
    }


def predict_joint_bayesian_model(
    pred_x, training_data, training_artifacts,
):
    # Retrieve quantities of interest
    posterior_draws = training_artifacts["posterior_draws"]
    normalization_constants = training_artifacts["normalization_constants"]

    # Normalize the training data and the prediction sites
    norm_training_data = normalize_training_data(training_data, normalization_constants)
    norm_pred_x = normalize(
        pred_x,
        normalization_constants["x_offset"],
        normalization_constants["x_scaling"],
    )

    # Problem dimensions
    num_predictions = pred_x.shape[0]
    dim_feature_space = posterior_draws["length_scales"].shape[1]
    num_posterior_draws = posterior_draws["length_scales"].shape[0]

    # Prediction
    norm_means, norm_variances = bfs_predict(
        norm_pred_x, posterior_draws, norm_training_data, dim_feature_space
    )

    # Denormalization and flattening of all stochastic dimensions
    means = denormalize(
        norm_means,
        normalization_constants["y_offset"],
        normalization_constants["y_scaling"],
    ).reshape((num_predictions, num_posterior_draws))

    variances = (norm_variances * normalization_constants["y_scaling"] ** 2).reshape(
        (num_predictions, num_posterior_draws)
    )

    return means, variances


def get_w_draws_bfs(
    training_data, validation_data, training_parameters, training_artifacts
):
    # Problem dimensions
    dim_input_space = training_data["x"].shape[1]
    dim_feature_space = training_parameters["dim_feature_space"]

    # Projection parameters posterior draws
    projection_parameters_draws = training_artifacts["posterior_draws"][
        "projection_parameters"
    ]
    num_posterior_draws = projection_parameters_draws.shape[0]

    # We transform all of these into a projection matrix
    w_draws = np.zeros((num_posterior_draws, dim_input_space, dim_feature_space))
    for i in range(num_posterior_draws):
        w_draws[i] = householder_reparameterization(
            projection_parameters_draws[i], dim_input_space, dim_feature_space
        )

    return w_draws
