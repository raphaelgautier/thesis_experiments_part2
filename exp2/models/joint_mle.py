from time import time
from functools import partial

from jax.scipy.linalg import cho_solve, cho_factor
from jax import numpy as jnp
import numpy as np
from numpyro import enable_x64
import pymanopt
from pymanopt import Problem
from pymanopt.manifolds import Euclidean, Stiefel, Product
from tqdm import tqdm

from .bgp import gp_kernel, bgp_predict_inner2
from .utils import (
    CustomConjugateGradient,
    normalize_training_data,
    normalize,
    denormalize,
)

enable_x64()

#############################################
# Multi-Fidelity GP Negative Log-Likelihood #
#############################################


def extract_model_parameters(optim_params):
    return {
        "w": optim_params[0],
        "noise_variance": jnp.power(10, optim_params[1][0]),
        "signal_variance": jnp.power(10, optim_params[1][1]),
        "length_scales": jnp.power(10, optim_params[1][2:]),
    }


def moas_log_likelihood(training_data, model_parameters, jitter=1e-8):
    # Unpacking training data and problem dimensions
    x, y = training_data["x"], training_data["y"]
    num_samples = x.shape[0]

    # Project training data
    z = training_data["x"] @ model_parameters["w"]

    # Covariance matrix
    k = (
        gp_kernel(z, z, model_parameters)
        + model_parameters["noise_variance"] * jnp.eye(num_samples)
        + jitter * jnp.eye(num_samples)
    )

    # Log likelihood
    c, lower = cho_factor(k, lower=True)
    alpha = cho_solve((c, lower), y)
    return (
        0.5 * y.T @ alpha
        + jnp.sum(jnp.log(jnp.diagonal(c)))
        + 0.5 * num_samples * jnp.log(2 * jnp.pi)
    )[0, 0]


def moas_negative_log_likelihood(training_data, w, log_model_parameters):
    # Unpacking the optim parameters
    model_parameters = extract_model_parameters((w, log_model_parameters))

    return -moas_log_likelihood(training_data, model_parameters)


########################
# Optimization Routine #
########################


def optimize_moas_model(
    problem,
    pymanopt_solver_params,
    num_restarts,
    get_x_0,
    random_seed_for_restarts=0,
):
    # Fixing seed for reproducibility
    np.random.seed(random_seed_for_restarts)

    # Training with restarts
    solver = CustomConjugateGradient(**pymanopt_solver_params)
    best_x, best_cost = None, np.inf
    with tqdm(range(num_restarts)) as progress_bar:
        for _ in progress_bar:
            x_0 = get_x_0()
            (w, log_model_parameters), cost = solver.solve(problem, x=x_0)
            progress_bar.set_postfix_str(
                f"cost = {cost:.2f}, best_cost = {best_cost:.2f}"
            )
            best_x, best_cost = (
                ((w, log_model_parameters), cost)
                if cost < best_cost
                else (best_x, best_cost)
            )
    # print(best_x)
    # Tighten it up
    print("Tightening it up...")
    solver = CustomConjugateGradient(use_cost_improvement_criterion=False)
    x, _ = solver.solve(problem, x=best_x)
    return x


######################################################
# High-Level Training/Prediction/Validation Routines #
######################################################


def train_joint_mle_model(training_data, training_parameters, dim_fs):
    # Extract relevant parameters
    optim_params = training_parameters["optim_params"]
    num_restarts = optim_params["num_restarts"]
    random_seed_for_restarts = optim_params["random_seed_for_restarts"]
    pymanopt_solver_params = optim_params["pymanopt_solver_params"]
    num_inputs = training_data["x"].shape[1]

    # Normalize training data
    normalized_training_data, normalization_constants = normalize_training_data(
        training_data
    )

    # Optimization
    start_time = time()

    projection_matrix_manifold = Stiefel(num_inputs, dim_fs)
    hyperparameters_manifold = Euclidean(2 + num_inputs)
    product_manifold = Product((projection_matrix_manifold, hyperparameters_manifold))
    cost_function = pymanopt.function.Jax(
        partial(moas_negative_log_likelihood, normalized_training_data)
    )
    problem = Problem(product_manifold, cost_function, verbosity=-1)

    def get_x_0():
        return (
            projection_matrix_manifold.rand(),
            np.concatenate(
                [
                    np.random.uniform(-2, 1, size=(1,)),  # noise variance
                    np.random.uniform(-1, 2, size=(1,)),  # signal variance
                    np.random.uniform(-1, 2, size=(dim_fs,)),  # len. scales
                ]
            ),
        )

    optim_params = optimize_moas_model(
        problem,
        pymanopt_solver_params,
        num_restarts,
        get_x_0,
        random_seed_for_restarts=random_seed_for_restarts,
    )
    model_parameters = extract_model_parameters(optim_params)

    training_duration = time() - start_time

    # Return training artifacts
    return {
        "dim_feature_space": dim_fs,
        "model_parameters": model_parameters,
        "normalization_constants": normalization_constants,
        "training_duration": training_duration,
    }


def predict_joint_mle_model(
    pred_x,
    training_data,
    training_artifacts,
    jitter=1e-8,
):
    # Extract parameters
    model_parameters = training_artifacts["model_parameters"]
    normalization_constants = training_artifacts["normalization_constants"]
    num_predictions = pred_x.shape[0]
    w = model_parameters["w"]

    # Normalize training data
    norm_training_data = normalize_training_data(training_data, normalization_constants)

    # Normalize prediction site locations
    norm_pred_x = normalize(
        pred_x,
        normalization_constants["x_offset"],
        normalization_constants["x_scaling"],
    )

    # We project the **normalized** inputs onto the FS
    norm_proj_training_data = {
        "x": norm_training_data["x"] @ w,
        "y": norm_training_data["y"],
    }
    x_star = norm_pred_x @ w

    # From there, it's standard MLE GP prediction
    x = norm_proj_training_data["x"]
    y = norm_proj_training_data["y"]
    eye = jnp.eye(x.shape[0])
    k = (
        gp_kernel(x, x, model_parameters)
        + model_parameters["noise_variance"] * eye
        + jitter * eye
    )
    c_and_lower = cho_factor(k, lower=True)
    alpha = cho_solve(c_and_lower, y)
    norm_means, norm_variances = bgp_predict_inner2(
        x_star,
        model_parameters,
        norm_proj_training_data,
        c_and_lower,
        alpha,
    )

    # Denormalization
    means = denormalize(
        norm_means,
        normalization_constants["y_offset"],
        normalization_constants["y_scaling"],
    ).reshape((num_predictions, 1))
    variances = (norm_variances * normalization_constants["y_scaling"] ** 2).reshape(
        (num_predictions, 1)
    )

    return means, variances


def get_w_draws_moas(
    training_data, validation_data, training_parameters, training_artifacts
):
    return training_artifacts["model_parameters"]["w"][None, ...]
