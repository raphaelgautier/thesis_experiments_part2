from functools import partial
import time

from jax import numpy as jnp
from jax.scipy.linalg import cho_factor, cho_solve

import numpy as np
from numpyro import enable_x64
import pymanopt
from pymanopt import Problem
from pymanopt.manifolds import Euclidean, Stiefel, Product
from tqdm import tqdm

from .bgp import bgp_model, gp_kernel
from .separate_simultaneous import predict_separate_simultaneous_model
from .utils import (
    mcmc,
    normalize_training_data,
    construct_w_orth,
    CustomConjugateGradient,
)

enable_x64()

#####################
# Training Routines #
#####################


def extract_model_parameters(optim_params, prev_w, w_orth):
    return {
        "w": jnp.concatenate((prev_w, w_orth @ optim_params[0]), axis=1),
        "noise_variance": jnp.power(10, optim_params[1][0]),
        "signal_variance": jnp.power(10, optim_params[1][1]),
        "length_scales": jnp.power(10, optim_params[1][2:]),
    }


def direction_finding_cost_function(
    training_data, prev_w, w_orth, direction, log_hyperparameters
):
    # Jitter
    jitter = 1e-8

    # Problem dimensions
    num_samples = training_data["x"].shape[0]

    # Extract model parameters
    model_parameters = extract_model_parameters(
        (direction, log_hyperparameters), prev_w, w_orth
    )

    # Project training data
    z = training_data["x"] @ model_parameters["w"]
    y = training_data["y"]

    # Covariance matrix
    k = (
        gp_kernel(z, z, model_parameters)
        + model_parameters["noise_variance"] * jnp.eye(num_samples)
        + jitter * jnp.eye(num_samples)
    )

    # Negative Log likelihood
    c, lower = cho_factor(k, lower=True)
    alpha = cho_solve((c, lower), y)
    return (
        0.5 * y.T @ alpha
        + jnp.sum(jnp.log(jnp.diagonal(c)))
        + 0.5 * num_samples * jnp.log(2 * jnp.pi)
    )[0, 0]


########################
# Optimization Routine #
########################


def train_seq_bfs_one_dim(
    norm_training_data, optim_params, mcmc_params, prev_w,
):

    # Extracting parameters
    num_restarts = optim_params["num_restarts"]
    random_seed_for_restarts = optim_params["random_seed_for_restarts"]
    pymanopt_solver_params = optim_params["pymanopt_solver_params"]
    dim_fs = prev_w.shape[1] + 1

    # Orthogonal to w
    w_orth = construct_w_orth(prev_w)

    # Fixing seed for reproducibility
    np.random.seed(random_seed_for_restarts)

    # Optimization
    direction_manifold = Stiefel(w_orth.shape[1], 1)
    hyperparameters_manifold = Euclidean(2 + dim_fs)
    product_manifold = Product((direction_manifold, hyperparameters_manifold))
    cost_function = pymanopt.function.Jax(
        partial(direction_finding_cost_function, norm_training_data, prev_w, w_orth)
    )
    problem = Problem(product_manifold, cost_function, verbosity=-1)

    # Funtion used to get a new optimization starting point
    def get_x_0():
        return (
            direction_manifold.rand(),
            np.concatenate(
                [
                    np.random.uniform(-2, 1, size=(1,)),  # noise variance
                    np.random.uniform(-1, 2, size=(1,)),  # signal variance
                    np.random.uniform(-1, 2, size=(dim_fs,)),  # len. scales
                ]
            ),
        )

    # Training with restarts
    solver = CustomConjugateGradient(**pymanopt_solver_params)
    best_x, best_cost = None, np.inf
    with tqdm(range(num_restarts)) as progress_bar:
        for _ in progress_bar:
            x_0 = get_x_0()
            x, cost = solver.solve(problem, x=x_0)
            progress_bar.set_postfix_str(
                f"cost = {cost:.2f}, best_cost = {best_cost:.2f}"
            )
            best_x, best_cost = (x, cost) if cost < best_cost else (best_x, best_cost)

    # Tighten it up
    print("Refining best solution...")
    solver = CustomConjugateGradient(use_cost_improvement_criterion=False)
    x, _ = solver.solve(problem, x=best_x)
    model_parameters = extract_model_parameters(x, prev_w, w_orth)
    w = model_parameters["w"]

    # Training of the Low-Dimensional Bayesian GP ######################################

    # Project training data on the FS
    proj_norm_training_data = {
        "x": norm_training_data["x"] @ w,
        "y": norm_training_data["y"],
    }

    # Train the Bayesian GP
    posterior_draws = mcmc(bgp_model, (proj_norm_training_data,), **mcmc_params,)

    return w, posterior_draws


###########################################
# High-Level Training/Prediction Routines #
###########################################


def train_separate_sequential_model(training_data, training_parameters, max_dim_fs):
    # Outputs Normalization ############################################################
    normalized_training_data, normalization_constants = normalize_training_data(
        training_data
    )

    # Initialization ###################################################################
    w = np.zeros((training_data["x"].shape[1], 0))
    training_duration = 0
    training_artifacts = []

    # Sequential training ##############################################################
    for dim_fs in range(1, max_dim_fs + 1):

        # We optimize for the i^th dimension
        start_time = time.time()
        w, posterior_draws = train_seq_bfs_one_dim(
            normalized_training_data,
            training_parameters["optim_params"],
            training_parameters["mcmc_params"],
            w,
        )
        this_training_duration = time.time() - start_time

        # The training duration of model at dim i is the cumulative training duration
        training_duration += this_training_duration

        # We save one training artifact per dimension
        training_artifacts.append(
            {
                "dim_feature_space": dim_fs,
                "normalization_constants": normalization_constants,
                "w": w,
                "posterior_draws": posterior_draws,
                "training_duration": training_duration,
            },
        )

    return training_artifacts


def predict_separate_sequential_model(pred_x, training_data, training_artifacts):
    """⚠ This is only for a single one of the models produced by the training routine.
    """
    return predict_separate_simultaneous_model(
        pred_x, training_data, training_artifacts
    )
