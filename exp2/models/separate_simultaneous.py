import time

from numpyro import enable_x64

from .bgp import bgp_predict, denormalize, bgp_model
from .joint_mle import train_joint_mle_model
from .utils import normalize, normalize_training_data, mcmc

enable_x64()


def train_separate_simultaneous_model(
    training_data, training_parameters, dim_fs, joint_mle_training_artifacts=None
):
    # 1. We find directions using a joint MLE model
    # `joint_mle_artifacts` is an optional parameter to avoid redundant training in
    # the experiment workflow
    if joint_mle_training_artifacts is None:
        joint_mle_training_artifacts = train_joint_mle_model(
            training_data, training_parameters["joint_mle"], dim_fs
        )

    # Extract training artifacts of interest
    w = joint_mle_training_artifacts["model_parameters"][
        "w"
    ]  # ⚠ this is a direction in the **normalized** x space
    normalization_constants = joint_mle_training_artifacts["normalization_constants"]
    joint_mle_training_duration = joint_mle_training_artifacts["training_duration"]

    # 2. We use the directions found using MLE to build a Bayesian GP model
    norm_training_data = normalize_training_data(training_data, normalization_constants)
    proj_norm_training_data = {
        "x": norm_training_data["x"] @ w,  # ⚠ projected in normalized space
        "y": norm_training_data["y"],
    }

    start_time = time.time()
    posterior_draws = mcmc(
        bgp_model, (proj_norm_training_data,), **training_parameters["mcmc_params"],
    )
    bgp_training_duration = time.time() - start_time

    return {
        "dim_feature_space": dim_fs,
        "w": w,
        "posterior_draws": posterior_draws,
        "normalization_constants": normalization_constants,
        "training_duration": joint_mle_training_duration + bgp_training_duration,
    }


def predict_separate_simultaneous_model(pred_x, training_data, training_artifacts):
    """ We rewrite something very similar to the BGP prediction routine because we need
    to project the **normalized** inputs onto the feature space.
    """
    # Retrieve relevant artifacts
    w = training_artifacts["w"]
    normalization_constants = training_artifacts["normalization_constants"]
    posterior_draws = training_artifacts["posterior_draws"]

    # Problem dimensions
    num_predictions = pred_x.shape[0]
    num_posterior_draws = posterior_draws["length_scales"].shape[0]

    # Normalize data
    norm_training_data = normalize_training_data(training_data, normalization_constants)
    norm_pred_x = normalize(
        pred_x,
        normalization_constants["x_offset"],
        normalization_constants["x_scaling"],
    )

    # Project data
    proj_norm_training_data = {
        "x": norm_training_data["x"] @ w,
        "y": norm_training_data["y"],
    }
    proj_norm_pred_x = norm_pred_x @ w

    # Predictions are made using the Bayesian GP
    norm_means, norm_variances = bgp_predict(
        proj_norm_pred_x, posterior_draws, proj_norm_training_data
    )

    # Denormalization and flattening of all stochastic dimensions
    means = denormalize(
        norm_means,
        normalization_constants["y_offset"],
        normalization_constants["y_scaling"],
    ).reshape((num_predictions, num_posterior_draws))

    variances = (norm_variances * normalization_constants["y_scaling"] ** 2).reshape(
        (num_predictions, num_posterior_draws)
    )

    return means, variances
