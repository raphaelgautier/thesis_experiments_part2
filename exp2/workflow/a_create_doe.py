__package__ = "exp2.workflow"

from itertools import product
from json import dump

from h5py import File

from .utils import get_data_dir

# DOE Parameters #######################################################################

doe_name = "exp2_20210326"
datasets = {
    "quadratic_function_10_inputs_1_active_dims": ["y"],
    "quadratic_function_10_inputs_2_active_dims": ["y"],
    "quadratic_function_10_inputs_5_active_dims": ["y"],
    "quadratic_function_25_inputs_1_active_dims": ["y"],
    "quadratic_function_25_inputs_2_active_dims": ["y"],
    "quadratic_function_25_inputs_5_active_dims": ["y"],
    "quadratic_function_50_inputs_1_active_dims": ["y"],
    "quadratic_function_50_inputs_2_active_dims": ["y"],
    "quadratic_function_50_inputs_5_active_dims": ["y"],
    "quadratic_function_100_inputs_1_active_dims": ["y"],
    "quadratic_function_100_inputs_2_active_dims": ["y"],
    "quadratic_function_100_inputs_5_active_dims": ["y"],
    "elliptic_pde": ["y_long", "y_short"],
    "hiv": ["y_3400"],
    "naca0012": ["lift", "drag"],
    "onera_m6": ["lift", "drag"],
    "CRM_450kgrid_twist_50DV_subsonic": [
        "lift",
        "drag",
        "moment_x",
        "moment_y",
        "moment_z",
        "sideforce",
    ],
    "CRM_450kgrid_twist_50DV_transonic": [
        "lift",
        "drag",
        "moment_x",
        "moment_y",
        "moment_z",
        "sideforce",
    ],
    "RAE2822_baseline_51DV_M0.725": ["lift", "drag", "moment_z"],
}

# DOE ranges
alt_num_training_samples_multiplicator = [1, 2, 3, 4, 5]
alt_dataset_split_random_seeds = [867, 57, 353, 601, 802]
alt_training_wrapper_names = [
    "joint_bayesian",
    "joint_mle_and_separate_simultaneous",
    "separate_sequential",
]
alt_dim_fs = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

# Process Paramaters ###################################################################

MCMC_PARAMETERS = {
    "target_acceptance_probability": 0.8,
    "num_chains": 1,
    "chain_method": "parallel",
    "num_warmup_draws": 500,
    "num_posterior_draws": 1000,
    "random_seed": 0,
    "progress_bar": True,
    "display_summary": True,
}

PYMANOPT_SOLVER_PARAMS = {
    "maxiter": 5000,
    "minstepsize": 1e-5,
    "mingradnorm": 2e-1,
    "cost_improvement_threshold": 1e-3,
    "no_cost_improvement_streak": 50,
}

OPTIM_PARAMS = {
    "num_restarts": 500,
    "random_seed_for_restarts": 0,
    "pymanopt_solver_params": PYMANOPT_SOLVER_PARAMS,
}

JOINT_MLE_PARAMS = {
    "optim_params": OPTIM_PARAMS,
}

PROCESS_PARAMETERS = {
    "training_wrapper_parameters": {
        "joint_bayesian": {"mcmc_params": MCMC_PARAMETERS},
        "joint_mle_and_separate_simultaneous": {
            "joint_mle": JOINT_MLE_PARAMS,
            "separate_simultaneous": {
                "joint_mle": JOINT_MLE_PARAMS,
                "mcmc_params": MCMC_PARAMETERS,
            },
        },
        "separate_sequential": {
            "optim_params": OPTIM_PARAMS,
            "mcmc_params": MCMC_PARAMETERS,
        },
    },
    "validation_parameters": {
        "confidence_interval_bounds_cdf_values": [0.025, 0.975],
        "num_gp_samples": 15,
        "random_seed": 0,
    },
}

# DOE Generation #######################################################################
cases = []
data_dir = get_data_dir()

# Iterate through each dataset
for dataset_name, output_names in datasets.items():
    # Retrieve needed dataset metadata
    with File(data_dir / "datasets" / f"{dataset_name}.h5", "r") as dataset:
        num_inputs = dataset["inputs"].shape[1]

    # Generate all cases for this dataset
    for (
        output_name,
        num_training_samples_multiplicator,
        dataset_split_random_seed,
        training_wrapper_name,
        dim_fs,
    ) in product(
        output_names,
        alt_num_training_samples_multiplicator,
        alt_dataset_split_random_seeds,
        alt_training_wrapper_names,
        alt_dim_fs,
    ):
        num_training_samples = int(num_inputs * num_training_samples_multiplicator)

        # For sep. seq., it would be redundant to train a model for each `dim_fs`
        if training_wrapper_name != "separate_sequential" or dim_fs == max(alt_dim_fs):
            cases.append(
                {
                    "dataset_name": dataset_name,
                    "output_name": output_name,
                    "num_training_samples": num_training_samples,
                    "dataset_split_random_seed": dataset_split_random_seed,
                    "training_wrapper_name": training_wrapper_name,
                    "dim_fs": dim_fs,
                }
            )

doe = {
    "process_parameters": PROCESS_PARAMETERS,
    "cases": dict(zip(range(len(cases)), cases)),
}

print(f"This DOE contains {len(cases)} cases.")
with open(data_dir / "does" / f"{doe_name}.json", "w") as doe_file:
    dump(doe, doe_file, indent=2)
