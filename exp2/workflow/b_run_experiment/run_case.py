from ...models.joint_bayesian import (
    train_joint_bayesian_model,
    predict_joint_bayesian_model,
)
from ...models.joint_mle import train_joint_mle_model, predict_joint_mle_model
from ...models.separate_sequential import (
    train_separate_sequential_model,
    predict_separate_sequential_model,
)
from ...models.separate_simultaneous import (
    train_separate_simultaneous_model,
    predict_separate_simultaneous_model,
)
from ..utils import (
    generic_validation_routine,
    load_dataset,
    save_model_artifacts,
    generate_reproducible_dataset_split,
)

#############################
# Wrapped Training Routines #
#############################

# 💡 The experiment workflow is organized to reduce computational redundancy


def wrapper_joint_bayesian(training_data, training_parameters, dim_fs):
    # Returns a single model
    training_artifacts = train_joint_bayesian_model(
        training_data, training_parameters, dim_fs
    )
    return [
        {
            "model_name": "joint_bayesian",
            "training_parameters": training_parameters,
            "training_artifacts": training_artifacts,
        }
    ]


def wrapper_joint_mle_and_separate_simultaneous(
    training_data, training_parameters, dim_fs
):
    # Returns two models: Joint MLE and Separate Simultaneous

    # Joint MLE
    joint_mle_training_parameters = training_parameters["joint_mle"]
    joint_mle_training_artifacts = train_joint_mle_model(
        training_data, joint_mle_training_parameters, dim_fs
    )

    # Separate Simultaneous
    sep_sim_training_parameters = training_parameters["separate_simultaneous"]
    sep_sim_training_artifacts = train_separate_simultaneous_model(
        training_data,
        sep_sim_training_parameters,
        dim_fs,
        joint_mle_training_artifacts=joint_mle_training_artifacts,
    )

    return [
        {
            "model_name": "joint_mle",
            "training_parameters": joint_mle_training_parameters,
            "training_artifacts": joint_mle_training_artifacts,
        },
        {
            "model_name": "separate_simultaneous",
            "training_parameters": sep_sim_training_parameters,
            "training_artifacts": sep_sim_training_artifacts,
        },
    ]


def wrapper_separate_sequential(training_data, training_parameters, dim_max_fs):
    # Returns `dim_max_fs` models, one for each `dim_fs`
    training_artifacts = train_separate_sequential_model(
        training_data, training_parameters, dim_max_fs
    )
    return [
        {
            "model_name": "separate_sequential",
            "training_parameters": training_parameters,
            "training_artifacts": artifacts,
        }
        for artifacts in training_artifacts
    ]


#####################
# Main Run Function #
#####################

TRAINING_WRAPPERS = {
    "joint_bayesian": wrapper_joint_bayesian,
    "joint_mle_and_separate_simultaneous": wrapper_joint_mle_and_separate_simultaneous,
    "separate_sequential": wrapper_separate_sequential,
}

PREDICTION_ROUTINES = {
    "joint_bayesian": predict_joint_bayesian_model,
    "joint_mle": predict_joint_mle_model,
    "separate_simultaneous": predict_separate_simultaneous_model,
    "separate_sequential": predict_separate_sequential_model,
}


def run_case(process_parameters, case_parameters):
    # Extract relevant info
    dataset_name = case_parameters["dataset_name"]
    output_name = case_parameters["output_name"]
    num_training_samples = case_parameters["num_training_samples"]
    dataset_split_random_seed = case_parameters["dataset_split_random_seed"]
    training_wrapper_name = case_parameters["training_wrapper_name"]
    dim_fs = case_parameters["dim_fs"]

    # Prepare training and validation datasets
    x, y, _ = load_dataset(dataset_name, output_name)
    training_indices, validation_indices = generate_reproducible_dataset_split(
        dataset_split_random_seed, num_training_samples, x.shape[0],
    )
    training_data = {"x": x[training_indices], "y": y[training_indices]}
    validation_data = {"x": x[validation_indices], "y": y[validation_indices]}

    # We also save the training and validation indices (just in case)
    case_parameters["training_indices"] = training_indices
    case_parameters["validation_indices"] = validation_indices

    # Retrieve training wrapper and run training
    # A training wrapper may output multiple models
    training_wrapper = TRAINING_WRAPPERS[training_wrapper_name]
    training_wrapper_parameters = process_parameters["training_wrapper_parameters"][
        training_wrapper_name
    ]
    training_wrapper_output = training_wrapper(
        training_data, training_wrapper_parameters, dim_fs,
    )

    # Validate models
    # Validation is performed for each individual model returned by the training wrapper
    for item in training_wrapper_output:
        # Extract wrapper output
        model_name = item["model_name"]
        training_parameters = item["training_parameters"]
        training_artifacts = item["training_artifacts"]

        # Need to keep track of the model name
        training_parameters["model_name"] = model_name

        # Perform validation
        validation_parameters = process_parameters["validation_parameters"]
        prediction_routine = PREDICTION_ROUTINES[model_name]
        validation_artifacts = generic_validation_routine(
            training_data,
            training_artifacts,
            prediction_routine,
            validation_data,
            validation_parameters,
        )

        # Filename
        artifact_name = "{}_{}_{}dFS_{}TS_{}RS_{}".format(
            dataset_name,
            output_name,
            training_artifacts["dim_feature_space"],
            num_training_samples,
            dataset_split_random_seed,
            model_name,
        )

        # Save model artifact
        save_model_artifacts(
            {
                "artifact_name": artifact_name,
                "case_parameters": case_parameters,
                "training_parameters": training_parameters,
                "training_artifacts": training_artifacts,
                "validation_parameters": validation_parameters,
                "validation_artifacts": validation_artifacts,
            }
        )
