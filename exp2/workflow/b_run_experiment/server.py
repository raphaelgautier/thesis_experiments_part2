from doe_scheduler.server import DoESchedulerServer

from ..utils import get_doe_scheduler_config

# Retrieve DOE scheduler config
doe_name, cases_list = get_doe_scheduler_config()

# Start the server
DoESchedulerServer(doe_name, cases_list).start()
