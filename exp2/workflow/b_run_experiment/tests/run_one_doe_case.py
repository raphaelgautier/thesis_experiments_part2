__package__ = "exp2.workflow.b_run_experiment.tests"

import argparse
import json

from ...utils import get_data_dir
from ..run_case import run_case


# Arguments
parser = argparse.ArgumentParser(description="Run a single Exp. 2 DOE case.")
parser.add_argument("-d", "--doe", required=True, help="name of the DOE")
parser.add_argument("-c", "--case", required=True, type=int, help="case number")
args = parser.parse_args()

if __name__ == "__main__":
    # Retrieve case info from DOE file
    with open(get_data_dir() / "does" / f"{args.doe}.json", "r") as doe_file:
        doe = json.load(doe_file)
        process_parameters = doe["process_parameters"]
        case_parameters = doe["cases"][str(args.case)]
        case_parameters["doe_name"] = args.doe

    # Run the case
    run_case(process_parameters, case_parameters)
