from exp2.workflow.b_run_experiment.run_case import run_case

# Case parameters ######################################################################
CASE_PARAMS = {
    "doe_name": "no_doe",
    "dataset_name": "naca0012",
    "output_name": "lift",
    "dim_feature_space": 3,
    "num_training_samples": 60,
    "dataset_split_random_seed": 0,
    # ["joint_bayesian", "joint_mle_and_sep_sim", "sep_seq"]
    "training_routine_name": "joint_bayesian",
}

# Process Paramaters ###################################################################

# Parameters used for MCMC (BFS and BGP)
MCMC_PARAMS = {
    "target_acceptance_probability": 0.8,
    "num_chains": 1,
    "chain_method": "parallel",
    "num_warmup_draws": 500,
    "num_posterior_draws": 1000,
    "random_seed": 0,
    "progress_bar": True,
    "display_summary": True,
}

# Parameters used for manifold optimization (MOAS)
PYMANOPT_SOLVER_PARAMS = {
    "maxiter": 5000,
    "minstepsize": 1e-5,
    "mingradnorm": 2e-1,
    "cost_improvement_threshold": 1e-3,
    "no_cost_improvement_streak": 50,
}

OPTIM_PARAMS = {
    "num_restarts": 500,
    "random_seed_for_restarts": 0,
    "pymanopt_solver_params": PYMANOPT_SOLVER_PARAMS,
}

PROCESS_PARAMS = {
    "training_parameters": {
        "joint_bayesian": {"mcmc_params": MCMC_PARAMS},
        "joint_mle": {"optim_params": OPTIM_PARAMS},
        "separate_simultaneous": {
            "optim_params": OPTIM_PARAMS,
            "mcmc_params": MCMC_PARAMS,
        },
        "separate_sequential": {
            "optim_params": OPTIM_PARAMS,
            "mcmc_params": MCMC_PARAMS,
        },
    },
    "validation_parameters": {
        "confidence_interval_bounds_cdf_values": [0.025, 0.975],
        "num_gp_samples": 15,
        "random_seed": 0,
    },
}


# Run case #############################################################################
run_case(PROCESS_PARAMS, CASE_PARAMS)
