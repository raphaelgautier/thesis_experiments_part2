__package__ = "exp2.workflow.b_run_experiment"

from json import load

from doe_scheduler.client import DoESchedulerClient
from socket import gethostname

from ..utils import get_worker_config
from .run_case import run_case


if __name__ == "__main__":
    # Retrieve worker config
    data_dir, doe_scheduler_server = get_worker_config()

    # The DOE scheduler client retrieves the next case number to run
    client = DoESchedulerClient(doe_scheduler_server, gethostname())
    doe_name, case_number = client.get_case()

    # We keep going as long as case numbers are given
    while case_number is not None:
        # Retrieve info
        with open(data_dir / "does" / f"{doe_name}.json", "r") as doe_file:
            doe = load(doe_file)
            process_parameters = doe["process_parameters"]
            case_parameters = doe["cases"][str(case_number)]
            case_parameters["doe_name"] = doe_name

        # Run the case, report if a problem occured
        try:
            run_case(process_parameters, case_parameters)
            client.report_finished()
        except Exception:
            client.report_failed()

        # Retrieve next case
        doe_name, case_number = client.get_case()
