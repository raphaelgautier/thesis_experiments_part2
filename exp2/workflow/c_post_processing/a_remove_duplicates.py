from pathlib import Path

import pandas as pd
from tqdm import tqdm

from exp2.workflow.utils import get_data_dir

# Parameters
doe_name = "exp2_20210326"

# Find duplicates and move duplicate files
results_dir = get_data_dir() / "results"
dir = Path(__file__).parent
models = pd.read_csv(dir / "run_models.csv")
duplicate_rows = models.duplicated(
    subset=[
        "dataset_name",
        "output_name",
        "num_training_samples",
        "dataset_split_random_seed",
        "model_name",
        "dim_fs",
    ]
)
duplicate_files = list(models[duplicate_rows]["file_path"].unique())
print(duplicate_files)
# for file in tqdm(duplicate_files):
#     old_path = Path(file)
#     relative_path = old_path.relative_to(results_dir / doe_name)
#     new_path = results_dir / f"{doe_name}_duplicates" / relative_path
#     new_path.parent.mkdir(exist_ok=True, parents=True)
#     old_path.rename(new_path)
