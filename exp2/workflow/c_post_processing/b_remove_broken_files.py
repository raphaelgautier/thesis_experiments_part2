from multiprocessing import Pool
from pathlib import Path

from h5py import File
from tqdm import tqdm

from exp2.workflow.utils import get_data_dir

# Parameters
doe_name = "exp2_20210326"


# Attempt to open results file, move it if it's broken
results_dir = get_data_dir() / "results"


def get_info(file_path):
    try:
        with File(file_path, "r") as _:
            pass
    except OSError:
        old_path = Path(file_path)
        relative_path = old_path.relative_to(results_dir / doe_name)
        new_path = results_dir / f"{doe_name}_broken" / relative_path
        new_path.parent.mkdir(exist_ok=True, parents=True)
        old_path.rename(new_path)


if __name__ == "__main__":
    # Enumerate all models that were produced
    files_list = list((get_data_dir() / "results" / doe_name).rglob("*.h5"))
    with Pool() as p:
        list(tqdm(p.imap(get_info, files_list), total=len(files_list)))
