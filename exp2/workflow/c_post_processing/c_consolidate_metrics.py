__package__ = "exp2.workflow.bc_reruns_failed_cases.batch1"

from multiprocessing import Pool
from pathlib import Path

from h5py import File
import numpy as np
import pandas as pd
from tqdm import tqdm

from exp2.workflow.utils import get_data_dir, compute_waic, compute_bic

# Parameters
doe_name = "exp2_20210326"


# Routine used to either extract the signal variance or compute the BIC
def get_noise_variance_bic_waic(h5_file):
    model_name = h5_file["training_parameters"].attrs["model_name"]

    if model_name in ["joint_bayesian", "separate_sequential", "separate_simultaneous"]:
        noise_variance = np.array(
            h5_file["training_artifacts"]["posterior_draws"]["noise_variance"]
        )
        bic = None
        waic = compute_waic(h5_file)
    elif model_name in ["joint_mle"]:
        noise_variance = np.array(
            h5_file["training_artifacts"]["model_parameters"].attrs["noise_variance"]
        )
        bic = compute_bic(h5_file)
        waic = None

    return noise_variance, bic, waic


# Open a file and retrieve relevant model info
def get_info(file_path):
    try:
        with File(file_path, "r") as h5_file:
            # Shortcuts to some dicts
            case_parameters = h5_file["case_parameters"].attrs
            validation_metrics = h5_file["validation_artifacts"]["validation_set"].attrs
            training_parameters = h5_file["training_parameters"].attrs
            training_artifacts = h5_file["training_artifacts"].attrs

            # Depending on what is applicable, retrieve distribution for signal
            # variance or compute BIC
            # noise_variance, bic, waic = get_noise_variance_bic_waic(h5_file)

            # Build the return dict
            return {
                # Case parameters
                "dataset_name": case_parameters["dataset_name"],
                "output_name": case_parameters["output_name"],
                "num_training_samples": case_parameters["num_training_samples"],
                "dataset_split_random_seed": case_parameters[
                    "dataset_split_random_seed"
                ],
                "model_name": training_parameters["model_name"],
                "dim_fs": training_artifacts["dim_feature_space"],
                # Point Results
                "r_squared": validation_metrics["r_squared"],
                "mlppd": validation_metrics["mlppd"],
                "training_duration": training_artifacts["training_duration"],
                # # Distributions
                # "noise_variance": noise_variance,
                # "bic": bic,
                # "waic": waic,
            }
    except OSError:
        print(f'Failed to open file "{file_path}".')


if __name__ == "__main__":
    # Enumerate all models that were produced
    files_list = list((get_data_dir() / "results" / doe_name).rglob("*.h5"))
    # files_list = files_list[0:50]
    with Pool() as p:
        models = list(tqdm(p.imap(get_info, files_list), total=len(files_list)))
    # Save models to csv
    models = [model for model in models if model is not None]
    pd.DataFrame(data=models).to_csv(Path(__file__).parent / "consolidated_results.csv")
