from multiprocessing import Pool

from tqdm import tqdm

from exp2.workflow.utils import get_data_dir
from exp2.workflow.d_plots.utils import plot_actual_vs_predicted, get_filenames

# DOE name
doe_name = "exp2_20210326"

# Retrieve data dir
data_dir = get_data_dir()

if __name__ == "__main__":
    cases = [
        # ("naca0012", "lift", 18),
        ("naca0012", "lift", 18 * 2),
        ("naca0012", "lift", 18 * 3),
        ("naca0012", "lift", 18 * 4),
        ("naca0012", "lift", 18 * 5),
    ]
    filenames = get_filenames(doe_name, cases)

    # Parallelization
    with Pool() as p:
        list(
            tqdm(
                p.imap_unordered(plot_actual_vs_predicted, filenames),
                total=len(filenames),
            )
        )
