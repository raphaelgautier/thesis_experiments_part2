from multiprocessing import Pool

from h5py import File
import pandas as pd
from tqdm import tqdm

from exp2.workflow.utils import get_data_dir

# Parameters
doe_name = "exp2_20210326"

# Retriev data dir
data_dir = get_data_dir()


# Open a file and retirve relevant model info
def get_info(file_path):
    try:
        with File(file_path, "r") as h5_file:
            case_parameters = h5_file["case_parameters"].attrs
            model_name = h5_file["training_parameters"].attrs["model_name"]
            dim_fs = h5_file["training_artifacts"].attrs["dim_feature_space"]
            return {
                "dataset_name": case_parameters["dataset_name"],
                "output_name": case_parameters["output_name"],
                "num_training_samples": case_parameters["num_training_samples"],
                "dataset_split_random_seed": case_parameters[
                    "dataset_split_random_seed"
                ],
                "model_name": model_name,
                "dim_fs": dim_fs,
                "file_path": file_path.relative_to(data_dir / "results"),
            }
    except OSError:
        print(f'Failed to open file "{file_path}".')


if __name__ == "__main__":
    # Enumerate all models that were produced
    files_list = list((get_data_dir() / "results" / doe_name).rglob("*.h5"))

    # Parallelization
    with Pool() as p:
        cases = list(
            tqdm(p.imap_unordered(get_info, files_list), total=len(files_list))
        )

    # Save index
    pd.DataFrame(cases).to_csv(data_dir / "results" / doe_name / "index_new.csv")
