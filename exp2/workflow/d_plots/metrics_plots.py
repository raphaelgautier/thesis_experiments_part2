from matplotlib import pyplot as plt
import pandas as pd
import seaborn as sns
from tqdm import tqdm

from exp2.workflow.utils import get_data_dir

doe_name = "exp2_20210326"


# data dir
data_dir = get_data_dir()

# Path to results file
file_path = data_dir / "results" / doe_name / "consolidated_results.csv"

# Read results file
results_df = pd.read_csv(file_path, index_col=0)

# Metrics
METRICS = ["r_squared", "mlppd", "training_duration"]

# Enumerate all plots
plots = []

dataset_names = results_df["dataset_name"].unique()
for dataset_name in dataset_names:
    results_ds = results_df[results_df["dataset_name"] == dataset_name]

    output_names = results_ds["output_name"].unique()
    for output_name in output_names:
        results_on = results_ds[results_ds["output_name"] == output_name]

        num_training_samples_list = results_on["num_training_samples"].unique()
        for num_training_samples in num_training_samples_list:
            # data = results_on[
            #     results_on["num_training_samples"] == num_training_samples
            # ]

            for metric in METRICS:
                plots.append((dataset_name, output_name, num_training_samples, metric))

# Make all plots
for plot in tqdm(plots):
    dataset_name, output_name, num_training_samples, metric = plot

    data = results_df[
        (results_df["dataset_name"] == dataset_name)
        & (results_df["output_name"] == output_name)
        & (results_df["num_training_samples"] == num_training_samples)
        & (results_df["model_name"] != "joint_mle")
    ]

    sns.boxplot(
        x="dim_fs",
        y=metric,
        hue="model_name",
        data=data,
        # ax=ax,
        linewidth=0.5,
        width=0.7,
        flierprops=dict(markerfacecolor="0.50", markersize=2),
    )
    plt.grid()
    plt.title(f"{dataset_name} - {output_name}\n{num_training_samples} TS")
    plt.xlabel("FS Dimension")
    plt.ylabel(metric)
    plt.tight_layout()
    output_dir = data_dir / "figures" / doe_name / dataset_name / output_name
    output_dir.mkdir(parents=True, exist_ok=True)
    plt.savefig(output_dir / f"{metric}_{num_training_samples}TS.pdf", format="pdf")
    plt.close()
