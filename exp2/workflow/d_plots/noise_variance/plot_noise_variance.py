from itertools import product

from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

from exp2.workflow.utils import get_data_dir

# Parameters
doe_name = "exp2_20210326"
dataset_name = "naca0012"
output_name = "lift"

# Retrieve data dir
data_dir = get_data_dir()

# Load WAIC
noise_variance = pd.read_csv(
    data_dir / "results" / doe_name / dataset_name / output_name / "noise_variance.csv",
    index_col=0,
    dtype={"noise_variance": np.array},
)

# For each dataset_name, output_name, num_training_samples, repetition,
# plot WAIC vs. number of FS dims, hue per model_name
for num_training_samples, dataset_split_random_seed in product(
    noise_variance["num_training_samples"].unique(),
    noise_variance["dataset_split_random_seed"].unique(),
):
    # Output directory
    out_dir = data_dir / "figures" / "noise_variance" / dataset_name / output_name
    out_dir.mkdir(parents=True, exist_ok=True)

    data = noise_variance[
        (noise_variance["num_training_samples"] == num_training_samples)
        & (noise_variance["dataset_split_random_seed"] == dataset_split_random_seed)
    ]
    print(np.array(data["noise_variance"].iloc[0]))

    # Plot
    sns.boxplot(
        data=data, x="dim_fs", y="noise_variance", hue="model_name",
    )
    plt.savefig(
        out_dir
        / "{}RS_{}TS.pdf".format(dataset_split_random_seed, num_training_samples)
    )
    plt.close()
