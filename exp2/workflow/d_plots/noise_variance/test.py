from itertools import groupby
from matplotlib import pyplot as plt
import numpy as np
import seaborn as sns

sns.boxplot(
    x=[0, 0, 0, 1, 1, 1, 2, 2, 2],
    y=[np.random.randn(30) for i in range(9)],
    hue=[0, 1, 2, 0, 1, 2, 0, 1, 2]
    #     np.array(
    #         [0 for i in range(10)] + [1 for i in range(10)] + [2 for i in range(10)]
    #     )
    #     for i in range(10)
    # ],
)
plt.show()
