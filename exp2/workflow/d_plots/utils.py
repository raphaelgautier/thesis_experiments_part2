from functools import reduce

import h5py
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd

from exp2.workflow.utils import get_data_dir, load_dataset

# Retrieve data dir
data_dir = get_data_dir()


def plot_actual_vs_predicted_asymmetric(
    actual_y,
    predicted_y,
    training_confidence_interval_bounds,
    title=None,
    xlabel="Actual",
    ylabel="Predicted",
    ax=None,
):
    errors = np.concatenate(
        (
            predicted_y - training_confidence_interval_bounds[:, 0:1],
            training_confidence_interval_bounds[:, 1:2] - predicted_y,
        ),
        axis=1,
    ).T

    mmin = min(np.min(actual_y), np.min(predicted_y))
    mmax = max(np.max(actual_y), np.max(predicted_y))
    padding = (mmax - mmin) * 0.1
    bounds = [mmin - padding, mmax + padding]

    if ax is None:
        fig, ax = plt.subplots(1, 1)

    ax.errorbar(actual_y, predicted_y, yerr=errors, fmt="o", alpha=0.2)

    ax.set_title("Predicted vs. Actual (Training)")
    ax.plot(bounds, bounds, "--")
    ax.set_aspect(1.0)
    ax.set_xlim(bounds)
    ax.set_ylim(bounds)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

    plt.grid()
    if title is not None:
        plt.title(title)
    plt.tight_layout()
    # plt.show()
    # plt.savefig('figures/training_actual_vs_predicted')

    return fig


def plot_actual_vs_predicted(results_filename):
    # Load relevant info from the results file
    with h5py.File(data_dir / "results" / results_filename, "r") as h5_file:
        dataset_name = h5_file["case_parameters"].attrs["dataset_name"]
        output_name = h5_file["case_parameters"].attrs["output_name"]
        model_name = h5_file["training_parameters"].attrs["model_name"]
        num_training_samples = h5_file["case_parameters"].attrs["num_training_samples"]
        doe_name = h5_file["case_parameters"].attrs["doe_name"]
        dataset_split_random_seed = h5_file["case_parameters"].attrs[
            "dataset_split_random_seed"
        ]
        dim_fs = h5_file["training_artifacts"].attrs["dim_feature_space"]

        training_indices = h5_file["case_parameters"]["training_indices"]
        validation_indices = h5_file["case_parameters"]["validation_indices"]

        training_point_based_predictions = h5_file["validation_artifacts"][
            "training_set"
        ]["point_based_predictions"]
        training_confidence_interval_bounds = h5_file["validation_artifacts"][
            "training_set"
        ]["confidence_interval_bounds"]

        validation_point_based_predictions = h5_file["validation_artifacts"][
            "validation_set"
        ]["point_based_predictions"]
        validation_confidence_interval_bounds = h5_file["validation_artifacts"][
            "validation_set"
        ]["confidence_interval_bounds"]

        # Load relevant info from the dataset file
        _, y_actual, _ = load_dataset(dataset_name, output_name)

        # Training and validation split
        training_y_actual = y_actual[training_indices]
        validation_y_actual = y_actual[validation_indices]

        # Output dir
        out_dir = (
            data_dir
            / "figures"
            / "actual_vs_predicted"
            / doe_name
            / dataset_name
            / output_name
        )
        out_dir.mkdir(parents=True, exist_ok=True)

        # Plots
        fig = plot_actual_vs_predicted_asymmetric(
            training_y_actual,
            training_point_based_predictions,
            training_confidence_interval_bounds,
            title=f"training - {model_name} - {num_training_samples} TS",
        )
        plt.savefig(
            out_dir
            / "training_{}_{}RS_{}TS_{}FS.pdf".format(
                model_name, dataset_split_random_seed, num_training_samples, dim_fs
            )
        )
        plt.close(fig)

        fig = plot_actual_vs_predicted_asymmetric(
            validation_y_actual,
            validation_point_based_predictions,
            validation_confidence_interval_bounds,
            title=f"validation - {model_name} - {num_training_samples} TS",
        )
        plt.savefig(
            out_dir
            / "validation_{}_{}RS_{}TS_{}FS.pdf".format(
                model_name, dataset_split_random_seed, num_training_samples, dim_fs
            )
        )
        plt.close(fig)


def get_filenames(doe_name, cases):
    index = pd.read_csv(data_dir / "results" / doe_name / "index.csv", index_col=0)
    filenames = []
    for case in cases:
        filenames.extend(
            index[
                (index["dataset_name"] == case[0])
                & (index["output_name"] == case[1])
                & (index["num_training_samples"] == case[2])
            ]["file_path"].tolist()
        )
    return filenames


def get_filenames_filter(doe_name, **kwargs):
    index = pd.read_csv(data_dir / "results" / doe_name / "index.csv", index_col=0)
    filter = reduce(
        lambda prev_filter, cv: prev_filter & (index[cv[0]] == cv[1]), kwargs.items(),
    )
    return index[filter]["file_path"].tolist()
