from multiprocessing import Pool

from h5py import File
import pandas as pd
from tqdm import tqdm

from exp2.workflow.utils import get_data_dir, compute_waic

# Parameters
doe_name = "exp2_20210326"
dataset_name = "naca0012"
output_name = "lift"

# Retrieve data dir
data_dir = get_data_dir()


# Open a file, compute BIC ro WAIC, and store it
def compute_bic_waic(file_path):
    try:
        with File(file_path, "r") as h5_file:
            # Shortcuts to some dicts
            case_parameters = h5_file["case_parameters"].attrs
            training_parameters = h5_file["training_parameters"].attrs
            training_artifacts = h5_file["training_artifacts"].attrs
            model_name = training_parameters["model_name"]

            lppd, p_waic_2, waic = compute_waic(h5_file)

            # # Depending on what is applicable, compute BIC/WAIC
            # model_name = training_parameters["model_name"]
            # if model_name in [
            #     "joint_bayesian",
            #     "separate_sequential",
            #     "separate_simultaneous",
            # ]:
            #     # 💡 We compute WAIC for Bayesian models
            #     waic = compute_waic(h5_file)
            #     bic = None
            # elif model_name in ["joint_mle"]:
            #     # 💡 BIC for MLE-based model
            #     bic = compute_bic(h5_file)
            #     waic = None

            return {
                # Case parameters
                "dataset_name": case_parameters["dataset_name"],
                "output_name": case_parameters["output_name"],
                "num_training_samples": case_parameters["num_training_samples"],
                "dataset_split_random_seed": case_parameters[
                    "dataset_split_random_seed"
                ],
                "model_name": model_name,
                "dim_fs": training_artifacts["dim_feature_space"],
                # BIC / WAIC
                # "bic": bic,
                "lppd": lppd,
                "p_waic_2": p_waic_2,
                "waic": waic,
            }
    except OSError:
        print(f'Failed to open file "{file_path}".')
    except Exception:
        print(
            "An non-OSError exception occured when computing BIC/WAIC for {}.".format(
                file_path
            )
        )


if __name__ == "__main__":
    # Retrieve files for which we want to compute WAIC
    index = pd.read_csv(data_dir / "results" / doe_name / "index.csv", index_col=0)
    filter = (
        (index["dataset_name"] == dataset_name)
        & (index["output_name"] == output_name)
        & (
            (index["model_name"] == "joint_bayesian")
            | (index["model_name"] == "separate_sequential")
            | (index["model_name"] == "separate_simultaneous")
        )
    )
    filenames = [
        data_dir / "results" / filename
        for filename in index[filter]["file_path"].tolist()
    ]
    # filenames = filenames[:10]
    # # Sequential
    # table = map(compute_bic_waic, tqdm(filenames))

    # Parallel
    with Pool() as p:
        table = list(
            tqdm(p.imap_unordered(compute_bic_waic, filenames), total=len(filenames))
        )

    # Save BIC/WAIC table to csv
    pd.DataFrame(table).to_csv(
        data_dir / "results" / doe_name / dataset_name / output_name / "waic.csv"
    )
