from itertools import product

from matplotlib import pyplot as plt
import pandas as pd
import seaborn as sns

from exp2.workflow.utils import get_data_dir

# Parameters
doe_name = "exp2_20210326"
dataset_name = "naca0012"
output_name = "lift"

# Retrieve data dir
data_dir = get_data_dir()

# Load WAIC
waic = pd.read_csv(
    data_dir / "results" / doe_name / dataset_name / output_name / "waic.csv",
    index_col=0,
)

# For each dataset_name, output_name, num_training_samples, repetition,
# plot WAIC vs. number of FS dims, hue per model_name
for num_training_samples, dataset_split_random_seed in product(
    waic["num_training_samples"].unique(), waic["dataset_split_random_seed"].unique()
):
    # Output directory
    out_dir = data_dir / "figures" / "waic" / dataset_name / output_name
    out_dir.mkdir(parents=True, exist_ok=True)

    # WAIC
    sns.lineplot(
        data=waic[
            (waic["num_training_samples"] == num_training_samples)
            & (waic["dataset_split_random_seed"] == dataset_split_random_seed)
        ],
        x="dim_fs",
        y="waic",
        hue="model_name",
    )
    plt.savefig(
        out_dir
        / "{}RS_{}TS_waic.pdf".format(dataset_split_random_seed, num_training_samples)
    )
    plt.close()

    # LPPD
    sns.lineplot(
        data=waic[
            (waic["num_training_samples"] == num_training_samples)
            & (waic["dataset_split_random_seed"] == dataset_split_random_seed)
        ],
        x="dim_fs",
        y="lppd",
        hue="model_name",
    )
    plt.savefig(
        out_dir
        / "{}RS_{}TS_lppd.pdf".format(dataset_split_random_seed, num_training_samples)
    )
    plt.close()

    # WAIC
    sns.lineplot(
        data=waic[
            (waic["num_training_samples"] == num_training_samples)
            & (waic["dataset_split_random_seed"] == dataset_split_random_seed)
        ],
        x="dim_fs",
        y="p_waic_2",
        hue="model_name",
    )
    plt.savefig(
        out_dir
        / "{}RS_{}TS_p_waic_2.pdf".format(
            dataset_split_random_seed, num_training_samples
        )
    )
    plt.close()
