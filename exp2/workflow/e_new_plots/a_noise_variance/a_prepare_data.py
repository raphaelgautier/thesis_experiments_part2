from itertools import product
import json
from multiprocessing import Pool
from pathlib import Path

from h5py import File
import numpy as np
import pandas as pd
from tqdm import tqdm


"""
In this script, we are building the data needed to create the plots of variance and R^2
vs. dimension.
At this point we are only looking at results for the joint Bayesian model.
"""


def get_groups(index):
    # Initialize list contraining the list of cases needed for a particular plot
    file_groups = []

    # We start by listing the h5 files we need for a particular plot
    print("Preparing groups...")

    # Iterate through all datasets
    for dataset_name in index["dataset_name"].unique():

        # Initialize filter based on dataset name
        dataset_filter = index["dataset_name"] == dataset_name

        # Iterate through all the outputs of this dataset
        for output_name in index[dataset_filter]["output_name"].unique():

            print(dataset_name, output_name)

            # Add filter based on output name
            output_name_filter = dataset_filter & (index["output_name"] == output_name)

            # Iterate through the number of training samples and repetitions
            # for this particular dataset and output
            for num_training_samples, dataset_split_random_seed in product(
                index[output_name_filter]["num_training_samples"].unique(),
                index[output_name_filter]["dataset_split_random_seed"].unique(),
            ):

                # Add filter based on number of training samples and repetition
                complete_filter = (
                    output_name_filter
                    & (index["num_training_samples"] == num_training_samples)
                    & (index["dataset_split_random_seed"] == dataset_split_random_seed)
                    & (index["model_name"] == "joint_bayesian")
                )
                rows = index[complete_filter]

                # Append all the remaining files as a group
                file_groups.append(
                    {
                        "dataset_name": dataset_name,
                        "output_name": output_name,
                        "num_training_samples": int(num_training_samples),
                        "dataset_split_random_seed": int(dataset_split_random_seed),
                        "files": [
                            "results/{}".format(filename)
                            for filename in rows["file_path"].tolist()
                        ],
                    }
                )
    return file_groups


def get_data(group):

    # Initialize group data
    data = []

    # Iterate through each file in the group
    for file in group["files"]:

        # Iterate through the files in the group, every file corresponds
        # to a dimension
        with File(data_dir / file, "r") as h5_file:
            noise_variance = h5_file["training_artifacts"]["posterior_draws"][
                "noise_variance"
            ]
            data.append(
                [
                    int(h5_file["training_artifacts"].attrs["dim_feature_space"]),
                    h5_file["validation_artifacts"]["validation_set"].attrs[
                        "r_squared"
                    ],
                    np.quantile(noise_variance, 0.025),
                    np.quantile(noise_variance, 0.975),
                    np.quantile(noise_variance, 0.5),
                ]
            )

    # Sort dy dimension
    data_array = np.array(data)
    data_array = data_array[data_array[:, 0].argsort()]

    group.update({"data": data_array.tolist()})

    return group


if __name__ == "__main__":
    # Retrieve data dir
    data_dir = Path("")

    # DOE name
    doe_name = "exp2_20210326"

    # Load index
    index = pd.read_csv(data_dir / "results" / doe_name / "index.csv", index_col=0)

    # Enumerate all models that were produced
    groups = get_groups(index)

    # Parallelization
    with Pool() as p:
        groups_with_data = list(
            tqdm(p.imap_unordered(get_data, groups), total=len(groups))
        )

    # # Save the groups
    # dir = data_dir / "figures" / doe_name
    # dir.mkdir(parents=True, exist_ok=True)
    # with open(dir / "data_noise_variance_plots.json", "w") as file:
    #     json.dump(groups_with_data, file, indent=2)
