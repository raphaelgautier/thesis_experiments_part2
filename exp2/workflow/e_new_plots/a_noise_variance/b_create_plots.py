from functools import partial
import json
from multiprocessing import Pool
from pathlib import Path

import matplotlib

matplotlib.use("pdf")

from matplotlib import pyplot as plt
import matplotlib.ticker as ticker
import numpy as np
from tqdm import tqdm


def change_axis_color(ax, color, side):
    ax.spines[side].set_color(color)
    ax.tick_params(axis="y", colors=color)
    ax.yaxis.label.set_color(color)
    # ax.title.set_color(color)


def create_plot(data_dir, doe_name, group):
    # Extract info
    dataset_name = group["dataset_name"]
    output_name = group["output_name"]
    num_training_samples = group["num_training_samples"]
    dataset_split_random_seed = group["dataset_split_random_seed"]
    data = np.array(group["data"])

    # Create figure
    _, ax = plt.subplots()
    ax2 = ax.twinx()
    change_axis_color(ax, "blue", "left")
    change_axis_color(ax2, "red", "right")

    # Noise variance
    ax.fill_between(data[:, 0], data[:, 2], data[:, 3], alpha=0.2)
    noise_variance = ax.plot(data[:, 0], data[:, 4], "b", label="Noise Variance")
    ax.set_xlabel("Feature Space Dimension")
    ax.set_ylabel("Noise Variance")
    ax.set_xlim([1, 10])

    # R^2
    r_squared = ax2.plot(data[:, 0], data[:, 1], "r", label="$R^2$")
    ax2.set_ylabel("$R^2$")
    ax2.yaxis.set_major_formatter(ticker.FormatStrFormatter("%.2f"))

    # Config figure
    plt.title(
        "{} - {}\n {} training samples - random seed = {}".format(
            dataset_name, output_name, num_training_samples, dataset_split_random_seed
        )
    )
    lines = noise_variance + r_squared
    labels = [line.get_label() for line in lines]
    ax.legend(lines, labels)
    plt.tight_layout()

    # Save figure
    dir = data_dir / "figures" / doe_name / "noise_variance_plots" / dataset_name
    dir.mkdir(parents=True, exist_ok=True)
    plt.savefig(
        dir
        / "{}_{}_{}_{}.pdf".format(
            dataset_name, output_name, num_training_samples, dataset_split_random_seed
        )
    )
    plt.close()


if __name__ == "__main__":
    # Retrieve data dir
    data_dir = Path("")

    # DOE name
    doe_name = "exp2_20210326"

    # Load the groups
    dir = data_dir / "figures" / doe_name
    dir.mkdir(parents=True, exist_ok=True)
    with open(dir / "data_noise_variance_plots.json", "r") as file:
        groups = json.load(file)

    # Parallelization
    with Pool() as p:
        list(
            tqdm(
                p.imap_unordered(partial(create_plot, data_dir, doe_name), groups),
                total=len(groups),
            )
        )
