from functools import partial
from multiprocessing import Pool
from pathlib import Path

import matplotlib

matplotlib.use("pdf")

from matplotlib import pyplot as plt
import matplotlib.ticker as ticker
import pandas as pd
from tqdm import tqdm

matplotlib.rc("font", size=8)
matplotlib.rc("xtick", labelsize=5)
matplotlib.rc("ytick", labelsize=5)


def change_axis_color(ax, color, side):
    ax.spines[side].set_color(color)
    ax.tick_params(axis="y", colors=color)
    ax.yaxis.label.set_color(color)


def create_groups(data):
    groups = []
    for dataset_name in data["dataset_name"].unique():
        for output_name in data[data["dataset_name"] == dataset_name][
            "output_name"
        ].unique():
            filtered_data = data[
                (data["dataset_name"] == dataset_name)
                & (data["output_name"] == output_name)
            ]
            groups.append(
                {
                    "dataset_name": dataset_name,
                    "output_name": output_name,
                    "data": filtered_data,
                }
            )
    return groups


def create_plot(
    i,
    j,
    ax,
    num_training_samples,
    dataset_split_random_seed,
    data,
):
    # TODO: reorganize for smaller plots and with new data structure

    # Create figure
    ax2 = ax.twinx()
    change_axis_color(ax, "blue", "left")
    change_axis_color(ax2, "red", "right")

    # Noise variance
    ax.fill_between(
        data["dim_feature_space"],
        data["lower_quantile_noise_variance"],
        data["upper_quantile_noise_variance"],
        alpha=0.2,
    )
    noise_variance = ax.plot(
        data["dim_feature_space"],
        data["median_noise_variance"],
        "b",
        label="Noise Variance",
    )
    if i == 4:
        ax.set_xticks([1, 4, 7, 10])
        ax.set_xlabel("FS Dim.\n $RS={}$".format(dataset_split_random_seed))
    else:
        ax.set_xticks([])
    if j == 0:
        ax.set_ylabel("{} TS\n$\\sigma_n$".format(num_training_samples))
    elif j == 4:
        ax2.set_ylabel("$R^2$")

    ax.set_xlim([1, 10])

    ax.ticklabel_format(
        axis="y", style="sci", scilimits=(0, 0), useOffset=False, useMathText=True
    )
    ax2.ticklabel_format(
        axis="y", style="sci", scilimits=(-2, 0), useOffset=False, useMathText=True
    )

    # R^2
    r_squared = ax2.plot(data["dim_feature_space"], data["r2"], "r", label="$R^2$")
    # ax2.yaxis.set_major_formatter(ticker.FormatStrFormatter("%.2f"))

    # Config figure
    # lines = noise_variance + r_squared
    # labels = [line.get_label() for line in lines]
    # ax.legend(lines, labels)


def create_5x5_plot(data_dir, doe_name, group):
    fig, ax = plt.subplots(5, 5, figsize=(9, 5))

    data = group["data"]
    # print(data)

    for i, num_training_samples in enumerate(
        sorted(data["num_training_samples"].unique())
    ):
        for j, dataset_split_random_seed in enumerate(
            sorted(data["dataset_split_random_seed"].unique())
        ):

            filtered_data = data[
                (data["num_training_samples"] == num_training_samples)
                & (data["dataset_split_random_seed"] == dataset_split_random_seed)
            ]
            filtered_data = filtered_data.sort_values(by="dim_feature_space")

            create_plot(
                i,
                j,
                ax[i, j],
                num_training_samples,
                dataset_split_random_seed,
                filtered_data,
            )

    # Figure title
    # plt.suptitle(
    #     "{} - {}".format(group["dataset_name"], group["output_name"]),
    #     y=0.98,
    #     verticalalignment="top",
    # )

    # Tight layout
    plt.tight_layout(pad=0.2, rect=(0.01, 0, 1, 1.0))

    # Save figure
    dir = data_dir / "figures" / doe_name / "noise_variance_5x5_plots"
    dir.mkdir(parents=True, exist_ok=True)
    plt.savefig(dir / "{}_{}.pdf".format(group["dataset_name"], group["output_name"]))
    plt.close(fig)


if __name__ == "__main__":
    # Retrieve data dir
    data_dir = Path("")

    # DOE name
    doe_name = "exp2_20210326"

    # File path
    file_path = data_dir / "figures" / doe_name / "data_5x5_noise_variance_plots.pkl"

    # Read prepared data
    data = pd.read_pickle(file_path)

    # Retrieve data organized plot-by-plot
    groups = create_groups(data)

    # Make the plot
    with Pool() as p:
        list(
            tqdm(
                p.imap_unordered(partial(create_5x5_plot, data_dir, doe_name), groups),
                total=len(groups),
            )
        )
