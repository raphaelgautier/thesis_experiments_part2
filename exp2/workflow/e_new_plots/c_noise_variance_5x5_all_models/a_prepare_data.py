from multiprocessing import Pool
from pathlib import Path

from h5py import File
import numpy as np
import pandas as pd
from tqdm import tqdm


"""
In this script, we are building the data needed to create the grid plots of variance and R^2 vs. dimension.
We are now looking at results for all models.
Each grid plot will contain the results for a given dataset and output, so it will be a 
5x5 grid since there are 5 repetition and 5 levels of training samples.
"""


def get_row(index_row_with_df_index):
    # Getting rid of the dataframe index
    _, index_row = index_row_with_df_index

    # Opening the H5 file and extracting relevant info
    file = "results/{}".format(index_row["file_path"])
    with File(data_dir / file, "r") as h5_file:
        # FS dimension
        dim_feature_space = int(
            h5_file["training_artifacts"].attrs["dim_feature_space"]
        )

        # R^2
        r2 = h5_file["validation_artifacts"]["validation_set"].attrs["r_squared"]

        if index_row["model_name"] == "joint_mle":
            median_noise_variance = h5_file["training_artifacts"][
                "model_parameters"
            ].attrs["noise_variance"]
            lower_quantile_noise_variance = 0
            upper_quantile_noise_variance = 0
        else:
            # Noise variance
            noise_variance = h5_file["training_artifacts"]["posterior_draws"][
                "noise_variance"
            ]
            lower_quantile_noise_variance = np.quantile(noise_variance, 0.025)
            upper_quantile_noise_variance = np.quantile(noise_variance, 0.975)
            median_noise_variance = np.quantile(noise_variance, 0.5)

    # Build the row and return
    return {
        "dataset_name": index_row["dataset_name"],
        "output_name": index_row["output_name"],
        "dataset_split_random_seed": index_row["dataset_split_random_seed"],
        "num_training_samples": index_row["num_training_samples"],
        "model_name": index_row["model_name"],
        "dim_feature_space": dim_feature_space,
        "lower_quantile_noise_variance": lower_quantile_noise_variance,
        "upper_quantile_noise_variance": upper_quantile_noise_variance,
        "median_noise_variance": median_noise_variance,
        "r2": r2,
    }


if __name__ == "__main__":
    # Retrieve data dir
    data_dir = Path("")

    # DOE name
    doe_name = "exp2_20210326"

    # Load index
    index = pd.read_csv(data_dir / "results" / doe_name / "index.csv", index_col=0)
    filtered_index = index[
        (index["model_name"] == "joint_bayesian")
        | (index["model_name"] == "joint_mle")
        | (index["model_name"] == "separate_sequential")
        | (index["model_name"] == "separate_simultaneous")
    ]
    index_rows = list(filtered_index.iterrows())

    # Parallelization
    with Pool() as p:
        rows = list(tqdm(p.imap_unordered(get_row, index_rows), total=len(index_rows)))

    # Save extracted data
    dir = data_dir / "figures" / doe_name
    dir.mkdir(parents=True, exist_ok=True)
    pd.DataFrame(rows).to_pickle(dir / "data_5x5_noise_variance_plots_all_models.pkl")
