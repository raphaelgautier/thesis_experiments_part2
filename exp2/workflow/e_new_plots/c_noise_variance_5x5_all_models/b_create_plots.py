from functools import partial
from multiprocessing import Pool
from pathlib import Path

import matplotlib

matplotlib.use("pdf")

from matplotlib import pyplot as plt
import matplotlib.ticker as ticker
from matplotlib.gridspec import GridSpec
import pandas as pd
from tqdm import tqdm

matplotlib.rc("font", size=8)
matplotlib.rc("xtick", labelsize=5)
matplotlib.rc("ytick", labelsize=5)


def change_axis_color(ax, color, side):
    ax.spines[side].set_color(color)
    ax.tick_params(axis="y", colors=color)
    ax.yaxis.label.set_color(color)


def create_groups(data):
    groups = []
    for dataset_name in data["dataset_name"].unique():
        for output_name in data[data["dataset_name"] == dataset_name][
            "output_name"
        ].unique():
            filtered_data = data[
                (data["dataset_name"] == dataset_name)
                & (data["output_name"] == output_name)
            ]
            groups.append(
                {
                    "dataset_name": dataset_name,
                    "output_name": output_name,
                    "data": filtered_data,
                }
            )
    return groups


MODEL_COLORS = {
    "joint_bayesian": "red",
    "joint_mle": "purple",
    "separate_simultaneous": "green",
    "separate_sequential": "blue",
}


def create_plot(
    i,
    j,
    ax,
    num_training_samples,
    dataset_split_random_seed,
    data,
):
    # TODO: reorganize for smaller plots and with new data structure

    # Create figure
    ax2 = ax.twinx()
    # change_axis_color(ax, "blue", "left")
    # change_axis_color(ax2, "red", "right")

    lines = []

    # Iterate through each method
    for model_name in [
        "joint_bayesian",
        "separate_simultaneous",
        "separate_sequential",
    ]:
        filtered_data = data[data["model_name"] == model_name]
        filtered_data = filtered_data.sort_values(by="dim_feature_space")

        # Noise variance
        if model_name != "joint_mle":
            ax.fill_between(
                filtered_data["dim_feature_space"],
                filtered_data["lower_quantile_noise_variance"],
                filtered_data["upper_quantile_noise_variance"],
                alpha=0.2,
                color=MODEL_COLORS[model_name],
            )
        (line,) = ax.plot(
            filtered_data["dim_feature_space"],
            filtered_data["median_noise_variance"],
            color=MODEL_COLORS[model_name],
            linestyle="solid",
            linewidth=0.7,
        )
        lines.append(line)

        # R^2
        (line,) = ax2.plot(
            filtered_data["dim_feature_space"],
            filtered_data["r2"],
            label="$R^2$",
            color=MODEL_COLORS[model_name],
            linestyle="dashed",
            linewidth=0.7,
        )
        lines.append(line)

    # Axes formatting
    if i == 4:
        ax.set_xticks([1, 4, 7, 10])
        ax.set_xlabel("FS Dim.\n $RS={}$".format(dataset_split_random_seed))
    else:
        ax.set_xticks([])
    if j == 0:
        ax.set_ylabel("{} TS\n$\\sigma_n$".format(num_training_samples))
    elif j == 4:
        ax2.set_ylabel("$R^2$")

    ax.set_xlim([1, 10])

    ax.ticklabel_format(
        axis="y", style="sci", scilimits=(0, 0), useOffset=False, useMathText=True
    )
    ax2.ticklabel_format(
        axis="y", style="sci", scilimits=(-2, 0), useOffset=False, useMathText=True
    )

    return lines


def create_5x5_plot(data_dir, doe_name, group):
    # Create figure
    fig = plt.figure(figsize=(9, 5))
    # fig, ax = plt.subplots(5, 5, figsize=(9, 5))

    # Retrieve data
    data = group["data"]
    # print(data)

    # Gridspec
    gs = GridSpec(
        6,
        5,
        height_ratios=[8, 8, 8, 8, 8, 1],
    )

    for i, num_training_samples in enumerate(
        sorted(data["num_training_samples"].unique())
    ):
        for j, dataset_split_random_seed in enumerate(
            sorted(data["dataset_split_random_seed"].unique())
        ):

            filtered_data = data[
                (data["num_training_samples"] == num_training_samples)
                & (data["dataset_split_random_seed"] == dataset_split_random_seed)
            ]
            # filtered_data = filtered_data.sort_values(by="dim_feature_space")

            # We retrieve the relevant axes using the grid spec
            ax = fig.add_subplot(gs[i, j])

            # We add the plot and keep track of lines for the legend
            lines = create_plot(
                i,
                j,
                ax,
                num_training_samples,
                dataset_split_random_seed,
                filtered_data,
            )

    # Add a single legend for all subplots
    legend_ax = fig.add_subplot(gs[5, :])
    legend_ax.legend(
        lines,
        [
            "Joint Bayesian - $\sigma_n$ (left axis)",
            "Joint Bayesian - $R^2$ (right axis)",
            "Joint MLE + B-GP - $\sigma_n$ (left axis)",
            "Joint MLE + B-GP - $R^2$ (right axis)",
            "Sequential - $\sigma_n$ (left axis)",
            "Sequential - $R^2$ (right axis)",
        ],
        mode=None,
        ncol=3,
        borderaxespad=0,
        loc="upper center",
        bbox_to_anchor=(0.5, -3.5),
    )
    legend_ax.axis("off")

    # Figure title
    # plt.suptitle(
    #     "{} - {}".format(group["dataset_name"], group["output_name"]),
    #     y=0.98,
    #     verticalalignment="top",
    # )

    # Tight layout
    # plt.tight_layout()
    plt.tight_layout(h_pad=-2.5, w_pad=-0.2)

    # Save figure
    dir = data_dir / "figures" / doe_name / "noise_variance_5x5_plots_all_models"
    dir.mkdir(parents=True, exist_ok=True)
    plt.savefig(dir / "{}_{}.pdf".format(group["dataset_name"], group["output_name"]))
    plt.close(fig)


if __name__ == "__main__":
    # Retrieve data dir
    data_dir = Path("")

    # DOE name
    doe_name = "exp2_20210326"

    # File path
    file_path = (
        data_dir / "figures" / doe_name / "data_5x5_noise_variance_plots_all_models.pkl"
    )

    # Read prepared data
    data = pd.read_pickle(file_path)

    # Retrieve data organized plot-by-plot
    groups = create_groups(data)

    # Make the plot
    with Pool() as p:
        list(
            tqdm(
                p.imap_unordered(partial(create_5x5_plot, data_dir, doe_name), groups),
                total=len(groups),
            )
        )
