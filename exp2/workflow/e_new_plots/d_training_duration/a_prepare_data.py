from multiprocessing import Pool
from pathlib import Path

from h5py import File
import pandas as pd
from tqdm import tqdm


"""
In this script, we are building the data needed to create the training duration plots.
"""


def create_groups(index):
    groups = []
    for dataset_name in index["dataset_name"].unique():
        this_dataset = index["dataset_name"] == dataset_name
        for output_name in index[this_dataset]["output_name"].unique():
            this_output = this_dataset & (index["output_name"] == output_name)
            for num_training_samples in index[this_output][
                "num_training_samples"
            ].unique():
                this_num_training_samples = this_output & (
                    index["num_training_samples"] == num_training_samples
                )
                for dataset_split_random_seed in index[this_num_training_samples][
                    "dataset_split_random_seed"
                ].unique():
                    this_repetition = this_num_training_samples & (
                        index["dataset_split_random_seed"] == dataset_split_random_seed
                    )
                    for model_name in index[this_repetition]["model_name"].unique():
                        this_model = this_repetition & (
                            index["model_name"] == model_name
                        )
                        rows = index[this_model]
                        groups.append(
                            {
                                "dataset_name": dataset_name,
                                "output_name": output_name,
                                "num_training_samples": num_training_samples,
                                "dataset_split_random_seed": dataset_split_random_seed,
                                "model_name": model_name,
                                "rows": rows,
                            }
                        )
    return groups


def get_row(group):
    # Training duration is stored differently depending on the model
    model_name = group["model_name"]

    if model_name in ["separate_simultaneous", "joint_bayesian"]:
        # We have to sum the training durations of all the models
        training_duration = 0
        for _, row in group["rows"].iterrows():
            file = "results/{}".format(row["file_path"])
            with File(data_dir / file, "r") as h5_file:
                training_duration += h5_file["training_artifacts"].attrs[
                    "training_duration"
                ]
    elif model_name == "separate_sequential":
        # We only have to get the training duration of the last model,
        # as it already includes the durations of the previous models
        max_fs_dim = group["rows"]["dim_fs"].max()
        row = group["rows"][group["rows"]["dim_fs"] == max_fs_dim].iloc[0]
        file = "results/{}".format(row["file_path"])
        with File(data_dir / file, "r") as h5_file:
            training_duration = h5_file["training_artifacts"].attrs["training_duration"]

    return {
        "dataset_name": group["dataset_name"],
        "output_name": group["output_name"],
        "dataset_split_random_seed": group["dataset_split_random_seed"],
        "num_training_samples": group["num_training_samples"],
        "model_name": group["model_name"],
        "training_duration": training_duration,
    }


if __name__ == "__main__":
    # Retrieve data dir
    data_dir = Path("")

    # DOE name
    doe_name = "exp2_20210326"

    # Load index
    index = pd.read_csv(data_dir / "results" / doe_name / "index.csv", index_col=0)
    filtered_index = index[index["model_name"] != "joint_mle"]
    groups = create_groups(filtered_index)

    # Parallelization
    with Pool() as p:
        rows = list(tqdm(p.imap_unordered(get_row, groups), total=len(groups)))

    # Save extracted data
    dir = data_dir / "figures" / doe_name
    dir.mkdir(parents=True, exist_ok=True)
    pd.DataFrame(rows).to_pickle(dir / "data_training_duration_plots_all_models.pkl")
