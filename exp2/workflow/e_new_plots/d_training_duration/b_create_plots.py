from functools import partial
from multiprocessing import Pool
from pathlib import Path

import matplotlib

matplotlib.use("pdf")

from matplotlib import pyplot as plt
import pandas as pd
import seaborn as sns
from tqdm import tqdm


matplotlib.rc("font", size=10)


def create_groups(data):
    groups = []
    for dataset_name in data["dataset_name"].unique():
        this_dataset = data["dataset_name"] == dataset_name
        for output_name in data[this_dataset]["output_name"].unique():
            this_output = this_dataset & (data["output_name"] == output_name)
            filtered_data = data[this_output]
            groups.append(
                {
                    "dataset_name": dataset_name,
                    "output_name": output_name,
                    "data": filtered_data,
                }
            )
    return groups


def create_plot(data_dir, doe_name, data_dict):
    # Retrieve data
    dataset_name = data_dict["dataset_name"]
    output_name = data_dict["output_name"]
    data = data_dict["data"]

    # Make plot
    hue_order = ["joint_bayesian", "separate_simultaneous", "separate_sequential"]
    palette = ["red", "green", "blue"]
    # print(data)

    _, ax = plt.subplots(figsize=(5, 3))
    ax = sns.lineplot(
        data=data,
        x="num_training_samples",
        y="training_duration",
        hue="model_name",
        hue_order=hue_order,
        palette=palette,
        alpha=0.2,
        ax=ax,
    )

    # Grid
    plt.grid(which="both")

    # Axes and plot titles
    plt.xlabel("Number of Training Samples")
    plt.ylabel("Training Duration (s)")
    # plt.title("{} - {}".format(dataset_name, output_name))

    # Log scale
    ax.set(yscale="log")

    # Legend
    handles, _ = ax.get_legend_handles_labels()
    ax.legend(
        handles=handles, labels=["Joint Bayesian", "Joint MLE + BGP", "Sequential"]
    )

    # Tight layout
    plt.tight_layout()

    # Save figure
    dir = data_dir / "figures" / doe_name / "training_duration"
    dir.mkdir(parents=True, exist_ok=True)
    plt.savefig(dir / "{}_{}.pdf".format(dataset_name, output_name))
    plt.close()


if __name__ == "__main__":
    # Retrieve data dir
    data_dir = Path("")

    # DOE name
    doe_name = "exp2_20210326"

    # File path
    file_path = (
        data_dir / "figures" / doe_name / "data_training_duration_plots_all_models.pkl"
    )

    # Read prepared data
    data = pd.read_pickle(file_path)

    # Retrieve data organized plot-by-plot
    groups = create_groups(data)

    # Make the plot
    with Pool() as p:
        list(
            tqdm(
                p.imap_unordered(partial(create_plot, data_dir, doe_name), groups),
                total=len(groups),
            )
        )
