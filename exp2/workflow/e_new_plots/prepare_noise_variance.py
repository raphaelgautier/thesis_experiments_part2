from itertools import product

from h5py import File
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from tqdm import tqdm

from exp2.workflow.utils import get_data_dir

# Parameters
doe_name = "exp2_20210326"
dataset_name = "naca0012"
output_name = "lift"

# Retrieve data dir
data_dir = get_data_dir()


if __name__ == "__main__":

    # Output directory
    out_dir = data_dir / "figures" / "noise_variance" / dataset_name / output_name
    out_dir.mkdir(parents=True, exist_ok=True)

    # Retrieve files for which we want to compute WAIC
    index = pd.read_csv(data_dir / "results" / doe_name / "index.csv", index_col=0)
    filter = (
        (index["dataset_name"] == dataset_name)
        & (index["output_name"] == output_name)
        & (
            (index["model_name"] == "joint_bayesian")
            | (index["model_name"] == "separate_sequential")
            | (index["model_name"] == "separate_simultaneous")
        )
    )
    filtered_index = index[filter]

    for num_training_samples, dataset_split_random_seed in tqdm(
        product(
            filtered_index["num_training_samples"].unique(),
            filtered_index["dataset_split_random_seed"].unique(),
        )
    ):

        rows = filtered_index[
            (filtered_index["num_training_samples"] == num_training_samples)
            & (filtered_index["dataset_split_random_seed"] == dataset_split_random_seed)
        ]

        filenames = [
            data_dir / "results" / filename for filename in rows["file_path"].tolist()
        ]

        # Data for this plot
        x = []
        y = []
        hue = []

        for filename in filenames:
            with File(filename, "r") as h5_file:
                x.append(h5_file["training_artifacts"].attrs["dim_feature_space"])
                y.append(
                    np.array(
                        h5_file["training_artifacts"]["posterior_draws"][
                            "noise_variance"
                        ]
                    )
                )
                hue.append(h5_file["training_parameters"].attrs["model_name"])

        sns.boxplot(x=x, y=y, hue=hue)

        plt.savefig(
            out_dir
            / "{}RS_{}TS.pdf".format(dataset_split_random_seed, num_training_samples)
        )
        plt.close()
