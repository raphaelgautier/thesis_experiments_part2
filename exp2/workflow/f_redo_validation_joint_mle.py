from functools import partial
from multiprocessing import Pool
from pathlib import Path

from h5py import File
import numpy as np
import pandas as pd
from tqdm import tqdm

from exp2.models.joint_mle import predict_joint_mle_model
from exp2.workflow.utils import (
    generic_validation_routine,
    pydict_to_h5group,
    h5group_to_pydict,
    load_dataset,
)


# Function to redo the validation for a given model
def redo_validation(data_dir, index_row_with_df_index):
    # Getting rid of the dataframe index
    _, index_row = index_row_with_df_index

    # Open the corresponding h5 file
    file = "results/{}".format(index_row["file_path"])
    with File(data_dir / file, "r") as h5_file:
        # Extracting needed info
        training_indices = np.array(h5_file["case_parameters"]["training_indices"])
        training_artifacts = h5group_to_pydict(h5_file["training_artifacts"])
        prediction_routine = predict_joint_mle_model
        validation_indices = np.array(h5_file["case_parameters"]["validation_indices"])
        validation_parameters = h5group_to_pydict(h5_file["validation_parameters"])

        # Retrieving training and validation data
        dataset_name = h5_file["case_parameters"].attrs["dataset_name"]
        output_name = h5_file["case_parameters"].attrs["output_name"]
        x, y, _ = load_dataset(dataset_name, output_name)
        training_data = {"x": x[training_indices], "y": y[training_indices]}
        validation_data = {"x": x[validation_indices], "y": y[validation_indices]}

        # Re-run the validation routine
        validation_artifacts = generic_validation_routine(
            training_data,
            training_artifacts,
            prediction_routine,
            validation_data,
            validation_parameters,
        )

    # Reopen file in write mode to correct the validation artifacts
    with File(data_dir / file, "r+") as h5_file:
        # Replace erroneous validation artifacts with the correct ones
        del h5_file["validation_artifacts"]
        validation_artifacts_group = h5_file.create_group("validation_artifacts")
        pydict_to_h5group(validation_artifacts, validation_artifacts_group)


if __name__ == "__main__":
    # Retrieve data dir
    data_dir = Path("")

    # DOE name
    doe_name = "exp2_20210326"

    # Load index
    index = pd.read_csv(data_dir / "results" / doe_name / "index.csv", index_col=0)
    filtered_index = index[
        (index["model_name"] == "joint_mle")
        # & (index["dataset_name"] == "CRM_450kgrid_twist_50DV_subsonic")
        # & (index["output_name"] == "drag")
    ]
    index_rows = list(filtered_index.iterrows())

    for index_row in tqdm(index_rows):
        redo_validation(data_dir, index_row)

    # # Parallelization
    # with Pool() as p:
    #     rows = list(
    #         tqdm(
    #             p.imap_unordered(partial(redo_validation, data_dir), index_rows),
    #             total=len(index_rows),
    #         )
    #     )
