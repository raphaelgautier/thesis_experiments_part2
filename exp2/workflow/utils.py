from configparser import ConfigParser
import datetime
from pathlib import Path
from socket import gethostname

import h5py
import jax
from jax import jit, numpy as jnp
from jax.random import PRNGKey
from jax.scipy.special import logsumexp
import numpy as np
from numpyro import distributions as dist
from scipy.linalg import svd, subspace_angles

from exp2.models.joint_mle import moas_log_likelihood, predict_joint_mle_model
from exp2.models.utils import normalize_training_data
from exp2.models.joint_bayesian import predict_joint_bayesian_model
from exp2.models.separate_sequential import predict_separate_sequential_model
from exp2.models.separate_simultaneous import predict_separate_simultaneous_model

#################
# Configuration #
#################

config = ConfigParser()
config.read(Path(__file__).parents[2] / ".config")


def get_data_dir():
    return Path(config["worker"]["data_dir"])


def get_worker_config():
    data_dir = get_data_dir()
    doe_scheduler_server = config["worker"]["server_url"]
    return data_dir, doe_scheduler_server


def get_doe_scheduler_config():
    doe_name = config["doe_scheduler_server"]["doe_name"]
    cases_list = list(
        range(
            int(config["doe_scheduler_server"]["cases_start"]),
            int(config["doe_scheduler_server"]["cases_end"]),
        )
    )
    return doe_name, cases_list


#################
# Error Metrics #
#################


@jit
def rmse_nrmse_r_squared_and_absolute_error(y_predicted, y_actual):
    var_y_actual = jnp.var(y_actual)
    absolute_error = y_predicted.reshape(-1) - y_actual.reshape(-1)
    mse = jnp.mean(jnp.square(absolute_error))
    r_squared = 1 - mse / var_y_actual
    rmse = jnp.sqrt(mse)
    nrmse = rmse / jnp.sqrt(var_y_actual)
    return rmse, nrmse, r_squared, absolute_error


@jit
def mean_log_pointwise_predictive_density_and_lppd(y_means, y_variances, y_actual):
    lppd = (
        logsumexp(
            dist.Normal(loc=y_means, scale=jnp.sqrt(y_variances)).log_prob(y_actual),
            axis=1,
        )
        - jnp.log(y_means.shape[1])
    )
    mlppd = jnp.mean(lppd, axis=0)
    return mlppd, lppd


####################
# Subspace Metrics #
####################


def compute_subspace_metrics(
    training_data,
    validation_data,
    training_parameters,
    training_artifacts,
    get_w_draws,
    dy_dx,
):
    # Retrieve samples of W from the method
    w_draws = get_w_draws(
        training_data, validation_data, training_parameters, training_artifacts
    )

    # Problem dimensions
    num_posterior_draws = w_draws.shape[0]
    dim_feature_space = w_draws.shape[2]

    # Compute the true AS
    true_w, _, _ = svd(np.transpose(dy_dx), full_matrices=False)
    true_w = true_w[:, :dim_feature_space]

    # Compute the subspace angles
    all_subspace_angles = np.empty((num_posterior_draws, dim_feature_space))
    for i in range(num_posterior_draws):
        all_subspace_angles[i] = subspace_angles(
            true_w,
            w_draws[i],
        )
    first_subspace_angle = all_subspace_angles[:, 0]
    mean_fsa = np.mean(first_subspace_angle)
    std_fsa = np.std(first_subspace_angle)

    return {
        "subspace_angles": all_subspace_angles,
        "first_subspace_angle": first_subspace_angle,
        "mean_fsa": mean_fsa,
        "std_fsa": std_fsa,
    }


######################
# Validation Metrics #
######################


def compute_validation_metrics(
    predicted_means,
    predicted_variances,
    actual_y,
    validation_parameters,
):
    # Quantile values need to be arrays
    confidence_interval_bounds_cdf_values = jnp.array(
        validation_parameters["confidence_interval_bounds_cdf_values"]
    )

    # We start with point-based predictions and confidence interval bounds
    # Depending on whether the method is fully Bayesian or not, the computations can be
    # made analytically or require sampling the posterior predictive distribution
    if predicted_means.shape[1] == 1:
        point_based_predictions = predicted_means
        confidence_interval_bounds = dist.Normal(
            predicted_means, jnp.sqrt(predicted_variances)
        ).icdf(confidence_interval_bounds_cdf_values)
    else:
        prng_key = PRNGKey(validation_parameters["random_seed"])
        num_samples = validation_parameters["num_gp_samples"]
        num_predictions = predicted_means.shape[0]
        num_posterior_draws = predicted_means.shape[1]
        samples = (
            dist.Normal(loc=predicted_means, scale=jnp.sqrt(predicted_variances))
            .sample(prng_key, sample_shape=(num_samples,))
            .transpose((1, 2, 0))
            .reshape((num_predictions, num_posterior_draws * num_samples))
        )

        quantiles = jnp.quantile(
            samples,
            jnp.concatenate((jnp.array([0.5]), confidence_interval_bounds_cdf_values)),
            axis=1,
        ).T

        point_based_predictions = quantiles[:, 0, None]
        confidence_interval_bounds = quantiles[:, 1:]

    # Global error metrics
    (
        rmse,
        nrmse,
        r_squared,
        absolute_pointwise_error,
    ) = rmse_nrmse_r_squared_and_absolute_error(point_based_predictions, actual_y)

    mlppd, lppd = mean_log_pointwise_predictive_density_and_lppd(
        predicted_means, predicted_variances, actual_y
    )

    return {
        "rmse": rmse,
        "nrmse": nrmse,
        "r_squared": r_squared,
        "mlppd": mlppd,
        "point_based_predictions": point_based_predictions,
        "confidence_interval_bounds": confidence_interval_bounds,
        "absolute_pointwise_error": absolute_pointwise_error,
        "lppd": lppd,
    }


##################################
# Persistence of the Run Results #
##################################


def compact_timestamp():
    return "{:%Y%m%d_%H%M%S}".format(datetime.datetime.now())


def pydict_to_h5group(pydict: dict, h5group: h5py.Group):
    for key, value in pydict.items():
        if isinstance(value, dict):
            subgroup = h5group.create_group(str(key))
            pydict_to_h5group(value, subgroup)
        else:
            if isinstance(value, jax.interpreters.xla.DeviceArray):
                value = np.array(value)

            if isinstance(value, np.ndarray):
                if value.size == 1:
                    h5group.attrs[key] = value.flatten()[0]
                else:
                    h5group.create_dataset(key, data=value)
            else:
                h5group.attrs[key] = value


def h5group_to_pydict(h5group: h5py.Group):
    d = dict()
    for key, value in h5group.items():
        if isinstance(value, h5py.Group):
            d[key] = h5group_to_pydict(value)
        elif isinstance(value, h5py.Dataset):
            d[key] = np.array(value)
        else:
            d[key] = value

    for key, value in h5group.attrs.items():
        d[key] = value

    return d


def save_model_artifacts(model_artifact):
    """Persist all case inputs, along with training and validation artifacts."""

    # Add additional info to the artifact ##############################################
    model_artifact["worker"] = gethostname()

    # Prepare a valid path for the artifact file #######################################

    # We organize artifacts into directories per dataset and per model
    doe_name = model_artifact["case_parameters"]["doe_name"]
    dataset_name = model_artifact["case_parameters"]["dataset_name"]
    output_name = model_artifact["case_parameters"]["output_name"]
    out_dir = get_data_dir() / "results" / doe_name / dataset_name / output_name
    out_dir.mkdir(parents=True, exist_ok=True)

    # Append timestamp if an artifact with the same name already exists
    artifact_name = model_artifact["artifact_name"]
    out_path = out_dir / f"{artifact_name}.h5"
    if out_path.is_file():
        out_path = out_dir / f"{artifact_name}_{compact_timestamp()}.h5"

    # Write the artifact to its file ###################################################
    with h5py.File(out_path, "w-") as artifact_file:
        pydict_to_h5group(model_artifact, artifact_file)


###############################
# Generic Validation Routines #
###############################


def validate_one_subset(
    data_to_validate,
    training_data,
    training_artifacts,
    prediction_routine,
    validation_parameters,
):
    means, variances = prediction_routine(
        data_to_validate["x"],
        training_data,
        training_artifacts,
    )
    validation_metrics = compute_validation_metrics(
        means,
        variances,
        data_to_validate["y"],
        validation_parameters,
    )
    return validation_metrics


def generic_validation_routine(
    training_data,
    training_artifacts,
    prediction_routine,
    validation_data,
    validation_parameters,
):
    # We compute both training and validation metrics
    validation_artifacts = {
        "training_set": validate_one_subset(
            training_data,
            training_data,
            training_artifacts,
            prediction_routine,
            validation_parameters,
        ),
        "validation_set": validate_one_subset(
            validation_data,
            training_data,
            training_artifacts,
            prediction_routine,
            validation_parameters,
        ),
    }

    return validation_artifacts


###################
# Dataset Loading #
###################


def load_dataset(dataset_name, output_name):
    with h5py.File(get_data_dir() / "datasets" / f"{dataset_name}.h5", "r") as dataset:
        x = np.array(dataset["inputs"])
        y = np.array(dataset["outputs"][output_name])
        dy_dx = (
            np.array(dataset["gradients"][output_name])
            if "gradients" in dataset
            else None
        )
    return x, y, dy_dx


########################################
# Reproducible Dataset Split Generator #
########################################


def generate_reproducible_dataset_split(
    dataset_split_random_seed, num_training_samples, num_total_samples
):
    np.random.seed(dataset_split_random_seed)  # fix random seed for reproducibility
    all_indices = np.arange(num_total_samples)
    training_indices = np.sort(
        np.random.choice(all_indices, num_training_samples, replace=False)
    )
    validation_indices = np.setdiff1d(all_indices, training_indices)
    return training_indices, validation_indices


################
# WAIC anc BIC #
################


@jit
def compute_p_waic_2(log_posterior_densities):
    # Axis 0 are training locations, axis 1 are MCMC posteriors
    # Eq. 7.12 p. 173 in Bayesian Data Analysis 3rd
    return jnp.sum(jnp.var(log_posterior_densities, axis=1), axis=0)


@jit
def compute_lppd(log_posterior_densities):
    # Eq. 7.5 p. 169 in Bayesian Data Analysis 3rd
    return jnp.sum(
        logsumexp(log_posterior_densities, axis=1)
        - jnp.log(log_posterior_densities.shape[1]),
        axis=0,
    )


@jit
def individual_lppds(means, variances, actual):
    # For a GP
    return dist.Normal(loc=means, scale=jnp.sqrt(variances)).log_prob(actual)


PREDICTION_ROUTINES = {
    "joint_bayesian": predict_joint_bayesian_model,
    "joint_mle": predict_joint_mle_model,
    "separate_simultaneous": predict_separate_simultaneous_model,
    "separate_sequential": predict_separate_sequential_model,
}


def compute_waic(h5_file):
    """Compute WAIC assuming that model is one of the Bayesian models."""

    # Retrieve training data
    training_indices = h5_file["case_parameters"]["training_indices"]
    dataset_name = h5_file["case_parameters"].attrs["dataset_name"]
    output_name = h5_file["case_parameters"].attrs["output_name"]
    x, y, _ = load_dataset(dataset_name, output_name)
    training_data = {"x": x[training_indices], "y": y[training_indices]}

    # Retrieve prediction routine
    model_name = h5_file["training_parameters"].attrs["model_name"]
    prediction_routine = PREDICTION_ROUTINES[model_name]

    # Retrieve training artifacts
    training_artifacts = {
        "normalization_constants": h5_file["training_artifacts"][
            "normalization_constants"
        ].attrs,
        "posterior_draws": {
            key: np.array(value)
            for key, value in h5_file["training_artifacts"]["posterior_draws"].items()
        },
    }
    if "w" in h5_file["training_artifacts"]:
        training_artifacts["w"] = h5_file["training_artifacts"]["w"]

    # Retrieve means and variances
    means, variances = prediction_routine(
        training_data["x"],
        training_data,
        training_artifacts,
    )

    # Compute WAIC
    # Eq. 7.13 p. 174 in Bayesian Data Analysis 3rd
    log_post_dens = individual_lppds(means, variances, training_data["y"])
    # print(means.shape, training_data["y"].shape, log_post_dens.shape)
    lppd = compute_lppd(log_post_dens)
    p_waic_2 = compute_p_waic_2(log_post_dens)
    waic = -2 * (lppd - p_waic_2)

    return lppd, p_waic_2, waic


def compute_bic(h5_file):
    """Computes BIC assuming the model is the joint MLE model."""
    # Compute log-likelihood
    training_indices = h5_file["case_parameters"]["training_indices"]
    dataset_name = h5_file["case_parameters"].attrs["dataset_name"]
    output_name = h5_file["case_parameters"].attrs["output_name"]
    x, y, _ = load_dataset(dataset_name, output_name)
    training_data = {"x": x[training_indices], "y": y[training_indices]}
    normalized_training_data = normalize_training_data(
        training_data,
        normalization_constants=h5_file["training_artifacts"][
            "normalization_constants"
        ].attrs,
    )
    mp = h5_file["training_artifacts"]["model_parameters"]
    model_parameters = {
        "length_scales": (
            np.array(mp["length_scales"])
            if "length_scales" in mp
            else np.array(mp.attrs["length_scales"])
        ),
        "noise_variance": np.array(mp.attrs["noise_variance"]),
        "signal_variance": np.array(mp.attrs["signal_variance"]),
        "w": np.array(mp["w"]),
    }
    log_likelihood = jit(moas_log_likelihood)(
        normalized_training_data, model_parameters
    )

    # Retrieve number of samples
    num_samples = h5_file["case_parameters"]["training_indices"].shape[0]

    # Compute number of parameters
    k = h5_file["training_artifacts"].attrs["dim_feature_space"]
    n = h5_file["training_artifacts"]["model_parameters"]["w"].shape[0]
    num_parameters = 2 + k + k * n - k * (k + 1) // 2

    # Compute BIC
    bic = -2 * log_likelihood + num_parameters * jnp.log(num_samples)

    return float(bic)
