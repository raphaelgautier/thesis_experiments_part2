from setuptools import setup

setup(
    name="ThesisExperimentsPart2",
    version="0.0.1",
    url="https://gitlab.com/raphaelgautier/thesis_experiments_part2",
    author="Raphael Gautier",
    author_email="raphael.gautier@gatech.edu",
    description="Second part of the code implementing thesis experiments.",
    packages=["exp2"],
    install_requires=[
        "jax==0.2.8",
        "tqdm==4.47.0",
        "numpy==1.18.5",
        "numpyro==0.5.0",
        "scipy==1.5.0",
        "pandas==1.0.5",
        "h5py==2.10.0",
    ],
)
